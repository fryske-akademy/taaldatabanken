package org.fa.tdb.conversie;

import name.dmaus.schxslt.SchematronException;
import net.sf.saxon.Configuration;
import net.sf.saxon.lib.DirectResourceResolver;
import net.sf.saxon.lib.ResourceRequest;
import net.sf.saxon.lib.StandardUnparsedTextResolver;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.Serializer;
import net.sf.saxon.s9api.XdmAtomicValue;
import net.sf.saxon.s9api.XdmNode;
import net.sf.saxon.s9api.XdmValue;
import net.sf.saxon.s9api.Xslt30Transformer;
import net.sf.saxon.s9api.XsltCompiler;
import net.sf.saxon.s9api.XsltExecutable;
import net.sf.saxon.trans.XPathException;
import org.apache.commons.lang3.StringUtils;
import org.fa.tei.validation.ValidationHelper;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.transform.Source;
import javax.xml.transform.SourceLocator;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import net.sf.saxon.s9api.MessageListener;

public class Conversie {

    private static final Logger LOGGER = Logger.getLogger(Conversie.class.getName());
    public static final String LOGGINGPROPERTIES = System.getProperty("user.dir") + File.separator + "ConversieLogging.properties";

    public static final String TDBEU="tdbeu";
    public static final String TDBLEKS ="tdbleks";

    private static final TextUriResolver TEXT_URI_RESOLVER = new TextUriResolver();

    public static void main(String[] args) throws Exception {
        if (new File(LOGGINGPROPERTIES).canRead()) {
            LogManager.getLogManager().readConfiguration(new FileInputStream(LOGGINGPROPERTIES));
        } else {
            Logger.getGlobal().warning(LOGGINGPROPERTIES + " not found, using built in configuration");
            LogManager.getLogManager().readConfiguration(Conversie.class.getResourceAsStream("/logging.properties"));
        }
        final XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();

        final MessageListener messageListener = new MessageListener() {
            @Override
            public void message(XdmNode xn, boolean bln, SourceLocator sl) {
                LOGGER.info(xn.toString());
            }
        };

        Processor processor = new Processor(Configuration.newConfiguration());
        final DirectResourceResolver directResourceResolver = new DirectResourceResolver(processor.getUnderlyingConfiguration()) {
            @Override
            public Source resolve(ResourceRequest request) throws XPathException {
                String uri = request.uri.toString();
                if (uri.startsWith("classpath:")) {
                    InputStreamReader reader = new InputStreamReader(
                            Conversie.class.getResourceAsStream(uri.toString().substring(10)));
                    return new SAXSource(new InputSource(reader));
                } else {
                    return super.resolve(request);
                }
            }
        };
        XsltCompiler xsltCompiler = processor.newXsltCompiler();
        InputStream xslt = Conversie.class.getResourceAsStream(
                TDBEU.equals(args[0]) ? "/xslt/convert-ynt-tdb.xslt" :
                        TDBLEKS.equals(args[0]) ? "/xslt/convert-leks-tdb.xslt" : null);
        final XsltExecutable conversieSheet = xsltCompiler.compile(new StreamSource(xslt));

        LOGGER.info("start conversie van " + args[1] + " naar " + args[2] + " met " + args[0] + " om " + new Date());

        Path destination = new File(args[2]).toPath();

        LOGGER.info("cleaning directory " + destination);

        if (Files.exists(destination, LinkOption.NOFOLLOW_LINKS)) {
            Files.walkFileTree(destination, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
        }

        destination.toFile().mkdir();

        LOGGER.info("converting");

        Files.walkFileTree(new File(args[1]).toPath(), new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                String d = dir.toString();
                if (d.contains("Wurdlist")) return FileVisitResult.SKIP_SUBTREE;
                int i = getRelativeIndex(d, destination);
                destination.resolve(d.substring(i)).toFile().mkdir();
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path source, BasicFileAttributes attrs) throws IOException {
                String f = source.toString();
                int i = getRelativeIndex(f, destination);
                Path newFile = destination.resolve(f.substring(i));
                if (newFile.equals(source)) {
                    throw new IOException("source file equals destination file: " + newFile.toString());
                }
                LOGGER.info("writing to " + newFile);

                newFile.getParent().toFile().mkdirs();

                if (!f.endsWith(".xml")) {
                    LOGGER.info("not " +
                            "converting just copying " + source);
                    Files.copy(source, newFile);
                    return FileVisitResult.CONTINUE;
                }
                convert(source, newFile, processor, conversieSheet, messageListener, directResourceResolver);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                return FileVisitResult.CONTINUE;
            }
        });
        LOGGER.info("conversion successful");

        LOGGER.info("einde conversie " + new Date());

    }

    public static void convert(Path source, Path newFile, Processor processor, XsltExecutable conversieSheet, MessageListener messageListener, DirectResourceResolver directResourceResolver) throws IOException {
        try {
            Map<QName, XdmValue> pars = new HashMap<>(1);
            if (!newFile.toFile().exists()) Files.createFile(newFile);
            Serializer serializer = processor.newSerializer(new BufferedOutputStream(new FileOutputStream(newFile.toFile())));
            pars.put(new QName("file"), new XdmAtomicValue(newFile.getFileName().toString()));
            pars.put(new QName("dir"), new XdmAtomicValue(newFile
                    .getParent().toAbsolutePath().toUri().toURL().toString()));
            final Xslt30Transformer transformer = conversieSheet.load30();
            transformer.setStylesheetParameters(pars);
            transformer.setMessageHandler(m -> messageListener.message(m.getContent(), m.isTerminate(), null));
            transformer.setUnparsedTextResolver(TEXT_URI_RESOLVER);
            transformer.setResourceResolver(directResourceResolver);
            transformer.applyTemplates(new StreamSource(new BufferedInputStream(new FileInputStream(source.toFile()))), serializer);
            ValidationHelper.validateXsdSchematron(newFile.toUri().toURL());
        } catch (SaxonApiException e) {
            LOGGER.severe("conversion failed at " + source);
            throw new RuntimeException(e);
        } catch (SAXException | SchematronException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
            throw new RuntimeException(ex);
        }
    }

    private static class TextUriResolver extends StandardUnparsedTextResolver {
        @Override
        public Reader resolve(URI absoluteURI, String encoding, Configuration config) throws XPathException {
            if (absoluteURI.toString().startsWith("classpath:")) {
                return new BufferedReader(new InputStreamReader(
                        Conversie.class.getResourceAsStream(absoluteURI.toString().substring(10))));
            } else {
                return super.resolve(absoluteURI, encoding, config);
            }
        }
    }

    private static int getRelativeIndex(String d, Path destination) {
        int i = StringUtils.indexOfDifference(destination.toString(), d);
        if (!d.substring(i-1,i).equals("/")) {
            for (int s = i-2; s > 0; s--) {
                if (d.substring(s,s+1).equals("/")) {
                    i = s+1;
                    break;
                }
            }
        }
        return i;
    }
}
