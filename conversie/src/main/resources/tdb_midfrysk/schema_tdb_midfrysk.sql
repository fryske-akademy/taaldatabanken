-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: tdb2
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auteur`
--

DROP TABLE IF EXISTS `auteur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auteur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `berne` char(10) DEFAULT NULL,
  `ferstoarn` char(10) DEFAULT NULL,
  `namme` char(255) DEFAULT NULL,
  `foarnamme` char(255) DEFAULT NULL,
  `slachte` char(1) DEFAULT NULL,
  `memmetaal` char(1) DEFAULT NULL,
  `opjeftetaal` char(1) DEFAULT '',
  `berteplak` char(255) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `skoalleplak` char(255) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `wenplak` char(255) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `opjefteplak` char(255) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `wenne_fan` char(4) DEFAULT NULL,
  `wenne_oant` char(4) DEFAULT NULL,
  `berteplak_mem` char(255) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `berteplak_heit` char(255) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `berop` char(255) DEFAULT NULL,
  `berop_heit` char(255) DEFAULT NULL,
  `koade` char(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `koade` (`koade`)
) ENGINE=MyISAM AUTO_INCREMENT=200298 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `autolem`
--

DROP TABLE IF EXISTS `autolem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autolem` (
  `db_id` int(11) NOT NULL DEFAULT '0',
  `form` char(255) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `pars` bigint(21) NOT NULL DEFAULT '0',
  `lemid` int(11) DEFAULT NULL,
  `parid` int(11) DEFAULT NULL,
  PRIMARY KEY (`db_id`,`form`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `boarnekomm`
--

DROP TABLE IF EXISTS `boarnekomm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `boarnekomm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namme` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `db_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `content` mediumtext NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `size` int(11) NOT NULL DEFAULT '0',
  `auteur` int(11) NOT NULL DEFAULT '0',
  `boekauteur` varchar(255) NOT NULL DEFAULT '',
  `titel` text NOT NULL,
  `boarne` text NOT NULL,
  `werprintinge` text NOT NULL,
  `tekstutjeften` text NOT NULL,
  `tekstutjeftetdb` text NOT NULL,
  `opmerking` text NOT NULL,
  `werprintingecomm` text NOT NULL,
  `werprintingefan` text NOT NULL,
  `werprintingefancomm` text NOT NULL,
  `jier` varchar(10) DEFAULT NULL,
  `h` char(1) DEFAULT NULL,
  `p` char(1) DEFAULT NULL,
  `letter` char(2) DEFAULT NULL,
  `lv` char(1) NOT NULL DEFAULT '',
  `oar` char(3) DEFAULT NULL,
  `utjouwer` int(11) DEFAULT NULL,
  `isoerset` char(1) DEFAULT NULL,
  `printinge` varchar(255) DEFAULT NULL,
  `wft` varchar(255) DEFAULT NULL,
  `udc` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `db_id` (`db_id`,`name`),
  UNIQUE KEY `jier` (`jier`,`db_id`,`name`)
) ENGINE=MyISAM AUTO_INCREMENT=304543 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `databank`
--

DROP TABLE IF EXISTS `databank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `databank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `public` tinyint(1) NOT NULL DEFAULT '0',
  `label` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `komment`
--

DROP TABLE IF EXISTS `komment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `komment` (
  `lemid` int(11) NOT NULL DEFAULT '0',
  `boarneid` int(11) NOT NULL DEFAULT '0',
  `side` int(11) NOT NULL DEFAULT '0',
  `db_id` int(11) NOT NULL DEFAULT '0',
  `komment` text NOT NULL,
  `oantal` int(11) NOT NULL DEFAULT '0',
  `jier` int(11) DEFAULT NULL,
  PRIMARY KEY (`lemid`,`boarneid`,`side`,`db_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `koppel`
--

DROP TABLE IF EXISTS `koppel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `koppel` (
  `Tdb_Id` int(11) NOT NULL DEFAULT '0',
  `Lemma_ID` int(11) NOT NULL DEFAULT '0',
  `rom` varchar(100) DEFAULT NULL,
  `form` varchar(100) NOT NULL DEFAULT '',
  `grammatica` varchar(100) DEFAULT NULL,
  `srtid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Tdb_Id`),
  KEY `Tdb_Id` (`Tdb_Id`),
  KEY `new_index` (`Lemma_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lemma`
--

DROP TABLE IF EXISTS `lemma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lemma` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `srtid` int(11) NOT NULL DEFAULT '0',
  `form` varchar(48) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `betsjutting` varchar(48) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `split` int(11) DEFAULT NULL,
  `lid1id` int(11) DEFAULT NULL,
  `lid2id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `form` (`form`,`srtid`,`betsjutting`)
) ENGINE=MyISAM AUTO_INCREMENT=224130 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `lemma2`
--

DROP TABLE IF EXISTS `lemma2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lemma2` (
  `lemma_id` int(11) NOT NULL DEFAULT '0',
  `db_id` int(11) NOT NULL DEFAULT '0',
  `form` varchar(255) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `typecnt` int(11) DEFAULT NULL,
  `tokencnt` int(11) DEFAULT NULL,
  `jier1` int(11) DEFAULT NULL,
  `jier2` int(11) DEFAULT NULL,
  PRIMARY KEY (`lemma_id`,`db_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `parad`
--

DROP TABLE IF EXISTS `parad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parad` (
  `srtid` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL DEFAULT '0',
  `parad` varchar(48) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `plak`
--

DROP TABLE IF EXISTS `plak`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plak` (
  `plaknamme` char(255) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `X` int(11) DEFAULT NULL,
  `Y` int(11) DEFAULT NULL,
  PRIMARY KEY (`plaknamme`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `soart`
--

DROP TABLE IF EXISTS `soart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `soart` (
  `id` tinyint(4) NOT NULL DEFAULT '0',
  `db_id` int(11) NOT NULL DEFAULT '0',
  `soart` varchar(48) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `token`
--

DROP TABLE IF EXISTS `token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token` (
  `token` varchar(48) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `bookid` int(11) NOT NULL DEFAULT '0',
  `pos` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`bookid`,`pos`),
  KEY `token` (`token`(6))
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `token2`
--

DROP TABLE IF EXISTS `token2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token2` (
  `typeid` int(11) NOT NULL DEFAULT '0',
  `bookid` int(11) NOT NULL DEFAULT '0',
  `pos` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`typeid`,`bookid`,`pos`),
  UNIQUE KEY `bookid` (`bookid`,`pos`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `type`
--

DROP TABLE IF EXISTS `type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `db_id` int(11) NOT NULL DEFAULT '0',
  `lemid` int(11) DEFAULT NULL,
  `parid` int(11) DEFAULT NULL,
  `form` varchar(255) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '',
  `amount` int(11) DEFAULT '0',
  `jier1` int(11) DEFAULT NULL,
  `jier2` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `db_id` (`db_id`,`lemid`,`parid`,`form`),
  KEY `form` (`form`(6))
) ENGINE=MyISAM AUTO_INCREMENT=2227770 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_roles` (
  `user_name` varchar(15) NOT NULL DEFAULT '',
  `role_name` varchar(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_name`,`role_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_name` varchar(255) NOT NULL DEFAULT '',
  `user_pass` varchar(255) NOT NULL DEFAULT '',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `email` varchar(255) DEFAULT NULL,
  `new_user` varchar(255) DEFAULT NULL,
  `ip` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`user_name`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `utjouwer`
--

DROP TABLE IF EXISTS `utjouwer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utjouwer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `namme` varchar(255) DEFAULT NULL,
  `adres` varchar(255) DEFAULT NULL,
  `postbus` varchar(255) DEFAULT NULL,
  `plak` varchar(255) DEFAULT NULL,
  `postk` varchar(6) DEFAULT NULL,
  `tillefoan` varchar(255) DEFAULT NULL,
  `oprjochte` varchar(255) DEFAULT NULL,
  `opheven` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=140 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-23 11:56:17
