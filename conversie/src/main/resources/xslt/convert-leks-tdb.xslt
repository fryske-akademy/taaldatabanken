<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                xmlns:fa="http://frisian.eu/tei-ud-linguistics"
                xmlns:conv="conversionNameSpace"
                exclude-result-prefixes="conv"
                version="3.0">

    <xsl:output method="xml" encoding="utf-8" indent="no"/>
    
    <xsl:template match="text()[following-sibling::*[1][name()='pc'][not(.='(' or .='[' or .='&lt;')] and preceding-sibling::*[1][name()='w']]" priority="3"/>

    <xsl:template match="text()[preceding-sibling::*[1][name()='pc'] and following-sibling::*[1][name()='pc']]" priority="2"/>

    <xsl:template match="text()[preceding-sibling::*[1][name()='pc' and (@type='apostrophe' or .='(' or .='[' or .='&lt;')]]" priority="1"/>

    <xsl:template match="text()[following-sibling::*[1][name()='pc' and (.=')' or .=']' or .='&gt;')]]"/>

    <xsl:template match="text()[preceding-sibling::*[1][name()='pc' and @type='quotation_mark'] and following-sibling::*[1][name()='w']]"/>

    <xsl:template match="text()[following-sibling::*[1][name()='pc' and @type='quotation_mark'][following-sibling::*[1][name()!='w']]]"/>

    <xsl:template match="@*|node()" priority="-1">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="tei:w/text()">
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>

    <xsl:template match="tei:w">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
        <xsl:value-of select="' '"/>
    </xsl:template>

    <xsl:template name="access">
        <xsl:param name="lang"/>
        <xsl:param name="keywords"/>
        <xsl:choose>
            <xsl:when test="starts-with($lang,'Njoggentjinde')">free</xsl:when>
            <xsl:when test="contains($lang,'Akadeemjestavering')">free</xsl:when>
            <xsl:when test="conv:lang($lang) = 'fry' and not($keywords/tei:term[contains(.,'bibel')])">restricted</xsl:when>
            <xsl:otherwise>free</xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:author"/>

    <xsl:template match="tei:TEI">
        <tei:TEI xmlns:fa="http://frisian.eu/tei-ud-linguistics" fa:linguisticsversion="4">
            <xsl:apply-templates select="node()"/>
        </tei:TEI>
    </xsl:template>

    <xsl:template match="tei:sourceDesc[../tei:titleStmt/tei:author]">
        <xsl:copy>
            <tei:listPerson>
                <tei:person role="author">
                    <tei:note type="info">information derived from and maintained in database!</tei:note>
                    <tei:persName><xsl:value-of select="../tei:titleStmt/tei:author"/> </tei:persName>
                </tei:person>
            </tei:listPerson>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="tei:publicationStmt">
        <xsl:copy>
            <xsl:apply-templates select="tei:publisher"/>
            <tei:availability>
                <xsl:attribute name="status">
                    <xsl:call-template name="access">
                        <xsl:with-param name="lang" select="//tei:language[1]/text()"/>
                        <xsl:with-param name="keywords" select="//tei:keywords"/>
                    </xsl:call-template>
                </xsl:attribute>
                <tei:licence target="http://creativecommons.org/licenses/by-sa/4.0/">
                    <tei:graphic url="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
                    This work is licensed under a
                    <tei:ref  target="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License.</tei:ref>
                </tei:licence>
            </tei:availability>
            <xsl:apply-templates select="tei:pubPlace"/>
            <xsl:apply-templates select="tei:date"/>
            <xsl:apply-templates select="tei:address"/>
        </xsl:copy>
        <tei:seriesStmt>
            <tei:title>
                <xsl:value-of select="../../tei:profileDesc/tei:langUsage/tei:language"/>
            </tei:title>
        </tei:seriesStmt>
    </xsl:template>
    
    <xsl:template match="tei:language">
        <tei:language ident="{conv:lang(text())}">
            <xsl:value-of select="."/>
        </tei:language>
    </xsl:template>

    <xsl:template match="tei:body">
        <xsl:copy>
            <xsl:attribute name="xml:lang" select="conv:lang(//tei:language[1]/text())"/>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="tei:teiHeader">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
            <tei:encodingDesc>
                <tei:tagsDecl>
                    <tei:rendition xml:id="bold" scheme="css">font-weight: bold</tei:rendition>
                    <tei:rendition xml:id="super" scheme="css">vertical-align: super</tei:rendition>
                </tei:tagsDecl>
                <tei:geoDecl datum="WGS84">World Geodetic System
                    <tei:geo><!-- TODO get longitude/latitude from file based on author and/or file name--></tei:geo>
                </tei:geoDecl>
            </tei:encodingDesc>
            <tei:revisionDesc>
                <tei:list>
                    <tei:item>initial version by conversion on
                        <tei:date when="{current-dateTime()}"/>
                    </tei:item>
                </tei:list>
            </tei:revisionDesc>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="@lemma[.='']"/>
    
  <xsl:function name="conv:lang" >
    <xsl:param name="code"/>
        
    <xsl:choose>
        <xsl:when test="$code=('ald','Aldfrysk')">ofs</xsl:when>
        <xsl:when test="$code = 'nij' or starts-with($code,'Nij')">fry</xsl:when>
        <xsl:when test="$code = 'Dialfoar1800'">fry-x-pre1800</xsl:when>
        <xsl:when test="$code = 'Hollands'">nl</xsl:when>
        <xsl:when test="$code = 'Dialekten'">fry-x-dial</xsl:when>
        <xsl:when test="$code = 'Harlingerlânsk'">frs-x-harl</xsl:when>
        <xsl:when test="$code = 'Runen'">ofs-x-runen</xsl:when>
        <xsl:when test="$code = 'Midfrysk'">fry-x-midfrysk</xsl:when>
        <xsl:when test="starts-with($code,'Selskipstavering')">fry</xsl:when>
        <xsl:when test="starts-with($code,'Njoggentjinde')">fry</xsl:when>
        <xsl:when test="$code='Bildtsk'">fry-x-biltsk</xsl:when>
        <xsl:when test="starts-with($code,'Âlde of Akadeemjestavering')">fry</xsl:when>
        <xsl:otherwise>TODO geen code voor <xsl:value-of select="$code"/></xsl:otherwise>
    </xsl:choose>

  </xsl:function>
</xsl:stylesheet>
