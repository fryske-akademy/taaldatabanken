<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tei="http://www.tei-c.org/ns/1.0"
                xmlns:fa="http://frisian.eu/tei-ud-linguistics"
                xmlns:conv="conversionNameSpace"
                version="3.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                exclude-result-prefixes="xsi">

    <xsl:import href="classpath:/xslt/grammar.xslt"/>

    <xsl:output method="xml" encoding="utf-8" indent="no"/>
        
    <xsl:param name="file"/>
    <xsl:param name="dir"/>
    <xsl:variable name="vertaling" select="doc('classpath:/lemmavertaling.xml')"/>
    <xsl:variable name="graphics" select="doc('classpath:/graphics/plaatjes.xml')"/>
    <xsl:variable name="apos">'</xsl:variable>
    <xsl:variable name="authorfile" select="unparsed-text('classpath:/xslt/auteurs.csv')"/>
    <xsl:variable name="authors">
        <xsl:for-each select="tokenize($authorfile,'\n')[position()>1]">
            <tei:person role="author">
                <tei:note type="info">information derived from and maintained in database!</tei:note>
<!--                data overnemen:-->
<!--                id|berne|ferstoarn|namme|foarnamme|slachte|||||wenplak||||||berop||koade-->
<!--                @key birth death persName/surname persName/forename sex/@value(M|F)  residence/placeName occupation idno-->
                <xsl:variable name="row" select="tokenize(.,'\|')"/>
                <xsl:for-each select="$row">
                    <xsl:variable name="pos" select="position()"></xsl:variable>
                    <xsl:choose>
                        <xsl:when test="$pos=2 and . != ''">
                            <tei:birth>
                                <xsl:choose>
                                    <xsl:when test="matches(.,'[0-9]+')">
                                        <xsl:attribute name="when" select="."/>
                                    </xsl:when>
                                    <xsl:otherwise><xsl:value-of select="."/> </xsl:otherwise>
                                </xsl:choose>
                            </tei:birth>
                        </xsl:when>
                        <xsl:when test="$pos=3 and . != ''">
                            <tei:death>
                                <xsl:choose>
                                    <xsl:when test="matches(.,'[0-9]+')">
                                        <xsl:attribute name="when" select="."/>
                                    </xsl:when>
                                    <xsl:otherwise><xsl:value-of select="."/> </xsl:otherwise>
                                </xsl:choose>
                            </tei:death>
                        </xsl:when>
                        <xsl:when test="$pos=4">
                            <tei:persName>
                                <tei:ref target="{$row[1]}">database key</tei:ref>
                                <xsl:if test=".!=''">
                                    <tei:forename><xsl:value-of select="."/> </tei:forename>
                                </xsl:if>
                                <xsl:if test="$row[5]!=''">
                                    <tei:surname><xsl:value-of select="$row[5]"/></tei:surname>
                                </xsl:if>
                            </tei:persName>
                        </xsl:when>
                        <xsl:when test="$pos=6 and . != ''">
                            <!--MFmfv-->
                            <tei:sex>
                                <xsl:attribute name="value">
                                    <xsl:choose>
                                        <xsl:when test=".='m' or .='M'">M</xsl:when>
                                        <xsl:when test=".='f' or .='F' or .='v'">F</xsl:when>
                                        <xsl:otherwise><xsl:value-of select="."/> </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:attribute>
                            </tei:sex>
                        </xsl:when>
                        <xsl:when test="$pos=11 and . != ''">
                            <tei:residence><tei:placeName><xsl:value-of select="."/></tei:placeName></tei:residence>
                        </xsl:when>
                        <xsl:when test="$pos=12 and $row[11] = '' and . != ''">
                            <tei:residence><tei:placeName type="given"><xsl:value-of select="."/></tei:placeName></tei:residence>
                        </xsl:when>
                        <xsl:when test="$pos=17 and . != ''">
                            <tei:occupation><xsl:value-of select="."/></tei:occupation>
                        </xsl:when>
                        <xsl:when test="$pos=19 and . != ''">
                            <tei:idno><xsl:value-of select="."/></tei:idno>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
            </tei:person>
        </xsl:for-each>

    </xsl:variable>

    <xsl:template match="w[subtitle]">
        <xsl:apply-templates select="node()"/>
    </xsl:template>
    
    <xsl:template name="idee">
        <xsl:param name="w"/>
        <xsl:choose>
            <xsl:when test="$w/@p">
                <xsl:value-of select="concat(replace(replace($w/@p,'[~= ^\[\]]','-'),$apos,'-'),'-',count($w/preceding::w))"/>
            </xsl:when>
            <xsl:when test="$w/@*">
                <xsl:value-of select="generate-id()"/>
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="w">
        <xsl:variable name="idee">
            <xsl:call-template name="idee">
                <xsl:with-param name="w" select="."/>
            </xsl:call-template>
        </xsl:variable>
        <tei:w>
            <xsl:if test="preceding-sibling::alang[1][.='la']">
                <xsl:attribute name="xml:lang" select="'la'"/>
            </xsl:if>
            <xsl:if test="@p">
                <xsl:attribute name="xml:id" select="$idee"/>
                <xsl:choose>
                    <xsl:when test="count(preceding-sibling::w[@p=current()/@p]) mod 2 = 1">
                        <xsl:attribute name="prev">
                            <xsl:value-of select="'#'"/>
                            <xsl:call-template name="idee">
                                <xsl:with-param name="w" select="preceding-sibling::w[@p=current()/@p][1]"/>
                            </xsl:call-template>
                        </xsl:attribute>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:attribute name="next">
                            <xsl:value-of select="'#'"/>
                            <xsl:call-template name="idee">
                                <xsl:with-param name="w" select="following-sibling::w[@p=current()/@p][1]"/>
                            </xsl:call-template>
                        </xsl:attribute>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
            <xsl:if test="@*">
                <xsl:call-template name="lijstjes">
                    <xsl:with-param name="w" select="."/>
                </xsl:call-template>
            </xsl:if>
            <xsl:apply-templates select="@l"/>
            <xsl:apply-templates select="node()"/>
            <xsl:if test="@krityske_edysje|@nijfrysk|@âldfrysk|@AF_lemma_HP|@ANW|@ynterpretaasje">
                <tei:app>
                    <xsl:apply-templates select="@krityske_edysje|@nijfrysk|@âldfrysk|@AF_lemma_HP|@ANW|@ynterpretaasje"/>
                </tei:app>
            </xsl:if>
        </tei:w>
        <xsl:apply-templates select="@p|@x|@falinsje">
            <xsl:with-param name="idee" select="$idee"/>
        </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="i">
        <xsl:comment>TODO TEI offers much more fine grained possibilities, see "Representation of Primary Sources" in TEI docs</xsl:comment>
        <tei:supplied reason="unknown">
            <xsl:apply-templates select="node()"/>
        </tei:supplied>
    </xsl:template>

    <xsl:template match="@l[.='']"/>

    <xsl:template match="@l">
        <xsl:choose>
            <xsl:when test="matches(.,'^[0-9]+$')">
                <xsl:variable name="lemma" select="id(concat('n',.),$vertaling)"/>
                <xsl:attribute name="lemma">
                    <xsl:value-of select="$lemma/orth"/>
                </xsl:attribute>
                <xsl:if test="count($lemma/orth) > 1">
                    <xsl:message>found multiple tei:orth entries (<xsl:value-of select="$lemma/orth"/>)</xsl:message>
                </xsl:if>
            </xsl:when>
            <xsl:when test="matches(.,'[0-9]$')">
                <xsl:variable name="lemma" select="replace(.,'[0-9]+$','')"/>
                <xsl:attribute name="lemma">
                    <xsl:value-of select="$lemma"/>
                </xsl:attribute>
                <xsl:message>Lemma (<xsl:value-of select="."/>) with sense in original: <xsl:value-of select="../preceding::text()[position() &lt; 10]"/>
                    <xsl:value-of select="concat('[',string-join(../text(),''),']')"/>
                    <xsl:value-of select="../following::text()[position() &lt; 10]"/>
                </xsl:message>
            </xsl:when>
            <xsl:otherwise>
                <xsl:attribute name="lemma">
                    <xsl:value-of select="."/>
                </xsl:attribute>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template
        match="@x | @falinsje">
        <xsl:param name="idee"/>
        <xsl:comment>Omitted attribute <xsl:value-of select="name()"/> with value: <xsl:value-of select="."/>.</xsl:comment>
    </xsl:template>

    <xsl:template
        match="@p[count(../preceding-sibling::w/@p[.=current()]) mod 2 = 1]">
        <xsl:param name="idee"/>
        <!-- we need to refer to two previously generated word idee's -->
        <xsl:variable name="w1">
            <xsl:call-template name="idee">
                <xsl:with-param name="w" select="../preceding-sibling::w[@p=current()][1]"/>
            </xsl:call-template>
        </xsl:variable>
        <tei:linkGrp type="UD-SYN" targFunc="head argument">
            <tei:link ana="compound:prt" target="#{$w1} #{$idee}"/>
        </tei:linkGrp>
    </xsl:template>

    <xsl:template
        match="@p[count(../following-sibling::w/@p[.=current()]) mod 2 = 1]"/>

    <xsl:template match="@*" priority="-1">
        <!-- This makes sure we don't miss any attributes in the conversion-->
        <xsl:attribute name="tei:noTeiFor_{name()}" select="."/>
    </xsl:template>

    <xsl:template match="*" priority="-1">
        <!-- This makes sure we don't miss any elements in the conversion-->
        <xsl:variable name="pad">
            <xsl:call-template name="pad">
                <xsl:with-param name="item" select="."/>
                <xsl:with-param name="pad" select="name()"/>
            </xsl:call-template>
        </xsl:variable>
        <tei:noTei>
            <xsl:attribute name="pad" select="$pad"/>
            <xsl:apply-templates select="@* | node()"/>
        </tei:noTei>
    </xsl:template>

    <xsl:template name="pad">
        <xsl:param name="item"/>
        <xsl:param name="pad"/>
        <xsl:choose>
            <xsl:when test="$item/..">
                <xsl:call-template name="pad">
                    <xsl:with-param name="pad" select="concat(name($item/..), '/', $pad)"/>
                    <xsl:with-param name="item" select="$item/.."/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$pad"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="@krityske_edysje">
        <tei:rdg type="critical_edition">
            <xsl:value-of select="."/>
        </tei:rdg>
    </xsl:template>

    <xsl:template match="@nijfrysk">
        <tei:rdg type="new_frisian">
            <xsl:value-of select="."/>
        </tei:rdg>
    </xsl:template>

    <xsl:template match="@âldfrysk">
        <tei:rdg type="old_frisian">
            <xsl:value-of select="."/>
        </tei:rdg>
    </xsl:template>

    <xsl:template match="@AF_lemma_HP">
        <tei:rdg type="af_hwb_hofmann_popkema">
            <xsl:value-of select="."/>
        </tei:rdg>
    </xsl:template>

    <xsl:template match="@ANW">
        <tei:rdg type="dutch">
            <xsl:value-of select="."/>
        </tei:rdg>
    </xsl:template>

    <xsl:template match="@ynterpretaasje">
        <tei:rdg type="interpretation">
            <xsl:value-of select="."/>
        </tei:rdg>
    </xsl:template>

    <!-- processed via pull -->
    <xsl:template
        match="@tiid | @modus | @tal | @slachte | @s | @persoan | @t | @wurdsoarte | @namfal | @type"/>

    <!-- omitted -->
    <xsl:template
        match="@transliteraasje | @transliteraasje_altnertafyf | @xl | @syntaktysk | @fleksje |doc/@id |doc/@nr">
        <xsl:message>
            <xsl:value-of select="concat('omitted: ', '@', name(), ' with value: ',.)"/>
        </xsl:message>
    </xsl:template>

    <!-- processed via pull -->
    <xsl:template match="alang|lg_num"/>

    <xsl:template match="heating|pre|headinf|subtitle[subtitle]">
        <xsl:message>
            <xsl:value-of select="concat('only processing children of: ', name())"/>
        </xsl:message>
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="heading">
        <xsl:comment>TODO This isn't very TEI like. See Default Text Structure in the TEI docs for ways to capture manuscript structure </xsl:comment>
        <tei:seg type="heading">
            <xsl:apply-templates select="@*|node()"/>
        </tei:seg>
    </xsl:template>

    <xsl:template match="heading2">
        <xsl:comment>TODO This isn't very TEI like. See Default Text Structure in the TEI docs for ways to capture manuscript structure </xsl:comment>
        <tei:seg type="heading2">
            <xsl:apply-templates select="@*|node()"/>
        </tei:seg>
    </xsl:template>

    <xsl:template match="b|h4">
        <xsl:comment>was <xsl:value-of select="name()"/></xsl:comment>
        <tei:seg rendition="#bold">
            <xsl:apply-templates select="@*|node()"/>
        </tei:seg>
    </xsl:template>

    <xsl:template match="span">
        <xsl:comment>TODO This isn't very TEI like. See Default Text Structure in the TEI docs for ways to capture manuscript structure </xsl:comment>
        <tei:seg type="span">
            <xsl:apply-templates select="@*|node()"/>
        </tei:seg>
    </xsl:template>

    <xsl:template match="lang">
        <xsl:comment>was element "lang", meaning "to the tune of"</xsl:comment>
        <tei:seg type="tune">
            <xsl:apply-templates select="@*|node()"/>
        </tei:seg>
        <tei:lb/>
    </xsl:template>

    <xsl:template match="sup">
        <xsl:comment>was <xsl:value-of select="name()"/></xsl:comment>
        <tei:seg rendition="#super">
            <xsl:apply-templates select="@*|node()"/>
        </tei:seg>
    </xsl:template>

    <xsl:template match="subtitle[not(subtitle)]">
        <xsl:comment>TODO This isn't very TEI like. See Default Text Structure in the TEI docs for ways to capture manuscript structure </xsl:comment>
        <tei:title type="subtitle">
            <xsl:apply-templates select="node()"/>
        </tei:title>
    </xsl:template>

    <xsl:template match="lg">
        <xsl:comment>TODO Alas the source does contain line group encoding, but no lines, so we cannot use TEI:lg </xsl:comment>
        <tei:seg type="lg">
            <xsl:if test="lg_num">
                <xsl:comment>number of line group: <xsl:value-of select="lg_num"/> </xsl:comment>
            </xsl:if>
            <xsl:apply-templates select="@* | node()"/>
        </tei:seg>
        <tei:lb/>
        <tei:lb/>
    </xsl:template>

    <xsl:template match="@utjouwer">
        <tei:publisher>
            <xsl:value-of select="."/>
        </tei:publisher>
    </xsl:template>

    <xsl:template match="@auteur">
        <xsl:variable name="auteur" select="$authors/tei:person[tei:persName/tei:ref[@target=current()]][1]"/>
            <xsl:choose>
            <xsl:when test="$auteur">
                <tei:listPerson>
                    <xsl:copy-of select="$authors/tei:person[tei:persName/tei:ref[@target=current()]][1]"/>
                </tei:listPerson>
            </xsl:when>
                <xsl:otherwise>
                    <tei:bibl><tei:note type="warn">author <xsl:value-of select="." /> not found</tei:note></tei:bibl>
                </xsl:otherwise>
            </xsl:choose>
    </xsl:template>

    <xsl:template match="@boekauteur"/>

    <xsl:template match="@opmerking">
        <tei:notesStmt>
            <tei:note>
                <xsl:value-of select="."/>
            </tei:note>
        </tei:notesStmt>
    </xsl:template>

    <xsl:template match="@tekstutjeften | @tekstutjeftetdb | @tekstutj">
        <tei:bibl type="{name(.)}">
            <xsl:value-of select="."/>
        </tei:bibl>
    </xsl:template>

    <xsl:template match="@bewarplak">
        <tei:placeName type="depository">
            <xsl:value-of select="."/>
        </tei:placeName>
    </xsl:template>

    <xsl:template match="@datearring">
        <tei:date type="dating">
            <xsl:value-of select="."/>
        </tei:date>
    </xsl:template>

    <xsl:template match="@fynplak">
        <tei:placeName type="site">
            <xsl:value-of select="."/>
        </tei:placeName>
    </xsl:template>

    <xsl:template match="@boarne | @signatuer">
        <xsl:comment>TODO see TEI docs on manuscript description for more fine grained options</xsl:comment>
        <tei:head>
            <xsl:value-of select="."/>
        </tei:head>
    </xsl:template>

    <xsl:template match="@printinge">
        <tei:bibl>
            <tei:note type="imprint_number">
                <xsl:value-of select="."/>
            </tei:note>
        </tei:bibl>
    </xsl:template>

    <xsl:template match="@werprintingecomm"/>

    <xsl:template match="@werprintinge">
        <tei:bibl>
            <xsl:if test="../@werprintingecomm">
                <tei:note type="reprinted_comment">
                    <xsl:value-of select="../@werprintingecomm"/>
                </tei:note>
            </xsl:if>
            <tei:note type="reprinted">
                <xsl:value-of select="."/>
            </tei:note>
        </tei:bibl>
    </xsl:template>

    <xsl:template match="@werprintingefancomm"/>

    <xsl:template match="@werprintingefan">
        <tei:bibl>
            <xsl:if test="../@werprintingefancomm">
                <tei:note type="reprint_of_comment">
                    <xsl:value-of select="../@werprintingefancomm"/>
                </tei:note>
            </xsl:if>
            <tei:note type="reprint_of">
                <xsl:value-of select="."/>
            </tei:note>
        </tei:bibl>
    </xsl:template>

    <xsl:template match="@materiaal">
        <tei:material>
            <xsl:value-of select="."/>
        </tei:material>
    </xsl:template>

    <xsl:template match="comment() | processing-instruction()">
        <xsl:copy-of select="."/>
    </xsl:template>
    
    <xsl:template match="text()">
        <xsl:call-template name="insertBreaks">
            <xsl:with-param name="pText" select="."/>
        </xsl:call-template>
    </xsl:template>
    
    <xsl:template name="insertBreaks">
        <xsl:param name="pText"/>

        <xsl:choose>
            <xsl:when test="not(contains($pText, '&#xA;'))">
                <xsl:copy-of select="$pText"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="substring-before($pText, '&#xA;')"/>
                <tei:lb />
                <xsl:call-template name="insertBreaks">
                    <xsl:with-param name="pText" select=
           "substring-after($pText, '&#xA;')"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
 
    <xsl:template match="aname">
        <xsl:comment>TODO This isn't very TEI like. See Default Text Structure in the TEI docs for ways to capture manuscript structure </xsl:comment>
        <tei:seg>
            <xsl:attribute name="type">
                <xsl:choose>
                    <xsl:when test="starts-with(.,'c')">chapter</xsl:when>
                    <xsl:when test="starts-with(.,'p')">paragraph</xsl:when>
                    <xsl:when test="starts-with(.,'l')">line</xsl:when>
                </xsl:choose>
            </xsl:attribute>
            <xsl:value-of select="."/>
        </tei:seg>
    </xsl:template>

    <xsl:template match="page">
        <!-- with aldfrysk the content of page isn't in the manuscript, otherwise it is -->
        <tei:pb>
            <xsl:choose>
                <xsl:when test="$graphics/plaatjes/plaatje[contains(@file,$file) and @page=current()]">
                    <xsl:attribute name="facs" select="$graphics/plaatjes/plaatje[contains(@file,$file) and @page=current()]/@graphic"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:comment>No graphic found for <xsl:value-of select="."/></xsl:comment>
                </xsl:otherwise>
            </xsl:choose>
        </tei:pb>
        <xsl:if test="not(contains($file,'17e_ieu'))">
            <xsl:choose>
                <xsl:when test="contains($file,'Aldfrysk')">
                    <tei:supplied reason="page">
                        <xsl:value-of select="."/>
                    </tei:supplied>
                </xsl:when>
                <xsl:otherwise>
                    <tei:seg type="page">
                        <xsl:value-of select="."/>
                    </tei:seg>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xsl:template match="doc/num">
        <xsl:comment>TODO This isn't very TEI like. See Default Text Structure in the TEI docs for ways to capture manuscript structure </xsl:comment>
        <tei:seg type="page">
            <xsl:value-of select="."/>
        </tei:seg>
    </xsl:template>

    <xsl:template name="tei">
        <tei:TEI xmlns:fa="http://frisian.eu/tei-ud-linguistics" fa:linguisticsversion="4">
            <xsl:comment>
                This document was initialy converted to customized TEI from xml in a format specific for the Fryske Akademy.
                See https://bitbucket.org/tei-encoding for the tei customizations used and https://bitbucket.org/taaldatabanken for the conversion.
                The source material was encoded with a strong focus on
                linguistic information, structure information is present but in a very limited way. Structure information that could not be translated to a
                TEI construct are translated to TEI:seg elements with a @type attribute indicating the original intention of the encoding.
                Linguistic information can be found in pos attribute and fa:* attributes on tei:w and tei:m elements.
            </xsl:comment>
            <tei:teiHeader>
                <tei:fileDesc>
                    <tei:titleStmt>
                        <tei:title><xsl:value-of select="/doc/@titel"/> </tei:title>
                    </tei:titleStmt>
                    <tei:publicationStmt>
                        <tei:publisher>
                            <tei:orgName>Fryske Akademy</tei:orgName>
                        </tei:publisher>
                        <xsl:call-template name="license"/>
                    </tei:publicationStmt>
                    <tei:seriesStmt>
                        <tei:title>
                            <xsl:value-of select="@databank"/>
                        </tei:title>
                    </tei:seriesStmt>
                    <xsl:apply-templates select="@opmerking"/>
                    <tei:sourceDesc>
                        <!-- here we can have listPerson with person/@role='author' and in it
                            birth/death/occupation/residence/education/langKnowledge


                        -->
                        <xsl:apply-templates select="@auteur"/>
                        <xsl:if
                            test="@materiaal or @boarne or @bewarplak or @fynplak or @datearring or @signatuer">
                            <tei:msDesc>
                                <tei:msIdentifier>
                                    <tei:repository><xsl:value-of select="if (@bewarplak) then @bewarplak else 'unknown'"/></tei:repository>
                                    <tei:msName>
                                        <xsl:value-of select="@titel"/>
                                    </tei:msName>
                                </tei:msIdentifier>
                                <xsl:apply-templates select="@boarne | @signatuer"/>
                                <xsl:if
                                    test="@materiaal or @bewarplak or @fynplak or @datearring">
                                    <tei:physDesc>
                                        <tei:objectDesc>
                                            <tei:supportDesc>
                                                <tei:support>
                                                    <xsl:apply-templates select="@bewarplak"/>
                                                    <xsl:apply-templates select="@fynplak"/>
                                                    <xsl:apply-templates select="@materiaal"/>
                                                    <xsl:apply-templates select="@datearring"/>
                                                </tei:support>
                                            </tei:supportDesc>
                                        </tei:objectDesc>
                                    </tei:physDesc>
                                </xsl:if>
                            </tei:msDesc>
                        </xsl:if>
                        <tei:bibl>
                            <xsl:apply-templates select="@utjouwer"/>
                            <tei:date>
                                <tei:note>This date originates from a @jier attribute in the
                                    original source, it looks like in most cases the date refers to the
                                    year the first edition of the manuscript saw the light.
                                </tei:note>
                                <xsl:value-of select="@jier"/>
                            </tei:date>
                        </tei:bibl>
                        <xsl:apply-templates select="@printinge"/>
                        <xsl:apply-templates select="@werprintinge"/>
                        <xsl:apply-templates select="@werprintingecomm"/>
                        <xsl:apply-templates select="@werprintingefan"/>
                        <xsl:apply-templates select="@werprintingefancomm"/>
                        <xsl:apply-templates select="@tekstutjeften | @tekstutjeftetdb | @tekstutj"/>
                    </tei:sourceDesc>
                </tei:fileDesc>
                <tei:encodingDesc>
                    <tei:tagsDecl>
                        <tei:rendition xml:id="bold" scheme="css">font-weight: bold</tei:rendition>
                        <tei:rendition xml:id="super" scheme="css">vertical-align: super</tei:rendition>
                    </tei:tagsDecl>
                </tei:encodingDesc>
                <tei:profileDesc>
                    <tei:langUsage>
                        <xsl:comment>TODO language codes / description</xsl:comment>
                        <tei:language ident="{conv:lang(@databank)}"><xsl:value-of select="@databank"/></tei:language>
                        <xsl:if test="//alang[text()='la']">
                            <tei:language ident="la">Latin</tei:language>
                        </xsl:if>
                    </tei:langUsage>
                </tei:profileDesc>
                <tei:revisionDesc>
                    <tei:list>
                        <tei:item>initial version by conversion on
                            <tei:date when="{current-dateTime()}"/>
                        </tei:item>
                    </tei:list>
                </tei:revisionDesc>
            </tei:teiHeader>
            <tei:text>
                <tei:body xml:lang="{conv:lang(@databank)}">
                    <tei:ab type="container">
                        <xsl:apply-templates select="node()"/>
                    </tei:ab>
                </tei:body>
            </tei:text>
        </tei:TEI>
    </xsl:template>
    
    <xsl:template name="license">
        <tei:availability status="free">
            <tei:licence target="http://creativecommons.org/licenses/by-sa/4.0/">
                <tei:graphic url="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />
                This work is licensed under a
                <tei:ref  target="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License.</tei:ref>
            </tei:licence>
        </tei:availability>
    </xsl:template>
    
  <xsl:function name="conv:lang" >
    <xsl:param name="code"/>
        
    <xsl:choose>
        <xsl:when test="$code=('ald','Aldfrysk')">ofs</xsl:when>
        <xsl:when test="$code = 'nij' or starts-with($code,'Nij')">fry</xsl:when>
        <xsl:when test="$code = 'Dialfoar1800'">fry-x-pre1800</xsl:when>
        <xsl:when test="$code = 'Hollands'">nl</xsl:when>
        <xsl:when test="$code = 'Dialekten'">fry-x-dial</xsl:when>
        <xsl:when test="$code = 'Harlingerlânsk'">frs-x-harl</xsl:when>
        <xsl:when test="$code = 'Runen'">ofs-x-runen</xsl:when>
        <xsl:when test="$code = 'Midfrysk'">fry-x-midfrysk</xsl:when>
        <xsl:when test="starts-with($code,'Selskipstavering')">fry</xsl:when>
        <xsl:when test="starts-with($code,'Njoggentjinde')">fry</xsl:when>
        <xsl:when test="$code='Bildtsk'">fry-x-biltsk</xsl:when>
        <xsl:when test="starts-with($code,'Âlde of Akadeemjestavering')">fry</xsl:when>
        <xsl:otherwise>TODO geen code voor <xsl:value-of select="$code"/></xsl:otherwise>
    </xsl:choose>

  </xsl:function>
  
    <xsl:template match="/doc">
        <xsl:message>omitting +PP and +NP in @t</xsl:message>
        <xsl:call-template name="tei"/>
    </xsl:template>
</xsl:stylesheet>
