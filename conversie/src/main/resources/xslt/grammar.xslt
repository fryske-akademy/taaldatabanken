<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:fa="http://frisian.eu/tei-ud-linguistics"
                version="2.0" >

    <xsl:output method="xml" encoding="utf-8"/>

    <xsl:template name="lijstjes">
        <xsl:param name="w"/>
        <xsl:variable name="val">
            <xsl:call-template name="anaValue">
                <xsl:with-param name="w" select="$w"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:if test="matches($val,'([^a-zA-Z][a-zA-Z]+[.]).*\1') or matches($val,'^([a-zA-Z]+[.]).* \1')">
            <xsl:message terminate="yes">multiple values in one linguistic category not allowed: <xsl:value-of select="$val"/>, atts: <xsl:value-of select="$w/@*"/>, w: <xsl:value-of select="$w"/></xsl:message>
        </xsl:if>
        <xsl:for-each select="tokenize($val,' ')">
            <xsl:if test="string-length(.) > 2">
                <xsl:variable name="kv" select="tokenize(.,'[.]')"/>
                <xsl:choose>
                    <xsl:when test="$kv[1]!='Pos'">
                        <xsl:attribute name="fa:{lower-case($kv[1])}" select="lower-case($kv[2])"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:attribute name="{lower-case($kv[1])}" select="lower-case($kv[2])"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="anaValue">
        <xsl:param name="w"/>
        <xsl:if test="$w/@t">
            <xsl:choose>
                <xsl:when test="contains($w/@t,'1-it-dt')">Person.1 Number.Sing <xsl:if test="not(contains($w/@t,'takom.tiid'))">Tense.Past </xsl:if></xsl:when>
                <xsl:when test="contains($w/@t,'1-it-nt')">Person.1 Number.Sing <xsl:if test="not(contains($w/@t,'takom.tiid'))">Tense.Pres </xsl:if></xsl:when>
                <xsl:when test="contains($w/@t,'1-mt-dt')">Person.1 Number.Plur <xsl:if test="not(contains($w/@t,'takom.tiid'))">Tense.Past </xsl:if></xsl:when>
                <xsl:when test="contains($w/@t,'1-mt-nt')">Person.1 Number.Plur <xsl:if test="not(contains($w/@t,'takom.tiid'))">Tense.Pres </xsl:if></xsl:when>
                <xsl:when test="contains($w/@t,'2-it-dt')">Person.2 Number.Sing <xsl:if test="not(contains($w/@t,'takom.tiid'))">Tense.Past </xsl:if></xsl:when>
                <xsl:when test="contains($w/@t,'2-it-nt')">Person.2 Number.Sing <xsl:if test="not(contains($w/@t,'takom.tiid'))">Tense.Pres </xsl:if></xsl:when>
                <xsl:when test="contains($w/@t,'2-mt-dt')">Person.2 Number.Plur <xsl:if test="not(contains($w/@t,'takom.tiid'))">Tense.Past </xsl:if></xsl:when>
                <xsl:when test="contains($w/@t,'2-mt-nt')">Person.2 Number.Plur <xsl:if test="not(contains($w/@t,'takom.tiid'))">Tense.Pres </xsl:if></xsl:when>
                <xsl:when test="contains($w/@t,'3-it-dt')">Person.3 Number.Sing  <xsl:if test="not(contains($w/@t,'takom.tiid'))">Tense.Past </xsl:if></xsl:when>
                <xsl:when test="contains($w/@t,'3-it-nt')">Person.3 Number.Sing  <xsl:if test="not(contains($w/@t,'takom.tiid'))">Tense.Pres </xsl:if></xsl:when>
                <xsl:when test="contains($w/@t,'3-mt-dt')">Person.3 Number.Plur <xsl:if test="not(contains($w/@t,'takom.tiid'))">Tense.Past </xsl:if></xsl:when>
                <xsl:when test="contains($w/@t,'3-mt-nt')">Person.3 Number.Plur <xsl:if test="not(contains($w/@t,'takom.tiid'))">Tense.Pres </xsl:if></xsl:when>
                <xsl:when test="contains($w/@t,'2-it')">Person.2 Number.Sing </xsl:when>
                <xsl:when test="contains($w/@t,'-it') and not($w/@s='it-subst.')">Number.Sing </xsl:when>
                <xsl:when test="contains($w/@t,'-mt')">Number.Plur </xsl:when>
                <xsl:when test="$w/@t='mt'">Number.Plur </xsl:when>
            </xsl:choose>
            <xsl:if test="matches($w/@t,'^[^k]?om') or matches($w/@t,'[^k]om')">VerbForm.Part Tense.Past </xsl:if>
            <xsl:if test="contains($w/@t,'fm')">VerbForm.Part Tense.Pres </xsl:if>
            <xsl:if test="contains($w/@t,'CONund') and not(contains($w/@t,'ûndersk'))">
                <xsl:message>not coding pos.sconj for atts: <xsl:value-of select="$w/@*"/>, w: <xsl:value-of select="$w"/></xsl:message>
            </xsl:if>
            <xsl:if test="contains($w/@t,'ûndersk')">Pos.SCONJ </xsl:if>
            <xsl:if test="contains($w/@t,'[lyksk.]')">Pos.CCONJ </xsl:if>
            <xsl:if test="contains($w/@t,'sels')">
                <xsl:choose>
                    <xsl:when test="$w/@s='adjektyf'">Pos.NOUN Convertedfrom.adj </xsl:when>
                    <xsl:when test="$w/@s='adverbium'">Pos.NOUN Convertedfrom.adv </xsl:when>
                    <xsl:when test="$w/@s='haadtw.'">Pos.NOUN Convertedfrom.ver </xsl:when>
                    <xsl:when test="$w/@s='numerale'">Pos.NOUN Convertedfrom.num </xsl:when>
                    <xsl:when test="$w/@s='pronomen'">Pos.NOUN Convertedfrom.pro </xsl:when>
                    <xsl:when test="$w/@s='it-subst.'">Pos.NOUN Number.Coll </xsl:when>
                </xsl:choose>
            </xsl:if>
            <xsl:if test="contains($w/@t,'[passyf]')">Voice.Pass </xsl:if>
            <xsl:if test="contains($w/@t,'[modaal]')">VerbType.Mod </xsl:if>
            <xsl:if test="contains($w/@t,'[folsl.tiid]')">VerbType.Tense </xsl:if>
            <xsl:if test="contains($w/@t,'takom.tiid')">Tense.Fut </xsl:if>
            <xsl:if test="contains($w/@t,'=nf')">VerbForm.Inf </xsl:if>
            <xsl:if test="contains($w/@t,'sup')">Degree.Sup </xsl:if>
            <xsl:if test="contains($w/@t,'komp')">Degree.Cmp </xsl:if>
            <xsl:if test="contains($w/@t,'rtw')">NumType.Ord </xsl:if>
            <xsl:if test="contains($w/@t,'htw')">NumType.Card </xsl:if>
            <xsl:if test="contains($w/@t,'hf')">Mood.Imp </xsl:if>
            <xsl:if test="contains($w/@t,'df')">VerbForm.Ger </xsl:if>
            <xsl:if test="contains($w/@t,'bwg')">VerbForm.Conv </xsl:if>
            <xsl:if test="contains($w/@t,'pgn')">Case.Par </xsl:if>
            <xsl:if test="contains($w/@t,'gn') and not(contains($w/@t,'pgn'))">Case.Gen </xsl:if>
            <xsl:if test="contains($w/@t,'emf')">PronType.Emp </xsl:if>
            <xsl:if test="contains($w/@t,'-in')">PronType.Ind </xsl:if>
            <xsl:if test="contains($w/@t,'pred')">Predicate.yes </xsl:if>
            <xsl:choose>
                <xsl:when test="contains($w/@t,'bytrans')">Valency.3 </xsl:when>
                <xsl:when test="contains($w/@t,'yntrans')">Valency.1 </xsl:when>
                <xsl:when test="contains($w/@t,'monotr')">Valency.2 </xsl:when>
                <xsl:when test="contains($w/@t,'[yntrans./trans]')">Valency.Tran </xsl:when>
            </xsl:choose>
            <xsl:if test="contains($w/@t,'nulfoarm')">
                <xsl:text>islemma.yes </xsl:text>
                <xsl:choose>
                    <xsl:when test="contains($w/@s,'tw.') and not(contains($w/@t,'=nf'))">VerbForm.Inf </xsl:when>
                    <xsl:when test="$w/@s = 'adjektyf'">Inflection.Uninf </xsl:when>
                </xsl:choose>
            </xsl:if>
            <xsl:choose>
                <xsl:when test="contains($w/@t,'-PRO')">prodrop.yes </xsl:when>
                <xsl:when test="contains($w/@t,'↔PRO')">clitic.yes </xsl:when>
                <xsl:when test="contains($w/@t,'?PRO')">clitic.yes </xsl:when>
            </xsl:choose>
            <xsl:if test="contains($w/@t,'fw')">Degree.dim </xsl:if>
            <xsl:choose>
                <xsl:when test="matches($w/@t,'-b[^w]') or matches($w/@t,'-b$') or matches($w/@t,'^b-')">Inflection.Infl </xsl:when>
                <xsl:when test="contains($w/@t,'nb-') or contains($w/@t,'-nb')">Inflection.Uninf </xsl:when>
            </xsl:choose>
            <xsl:if test="contains($w/@t,'attr')">Construction.Attr </xsl:if>
        </xsl:if>
        <xsl:if test="$w/@s[not(contains($w/@t,'sels'))]">
            <xsl:choose>
                <xsl:when test="$w/@s='adjektyf'">Pos.ADJ</xsl:when>
                <xsl:when test="$w/@s='adverbium'">Pos.ADV</xsl:when>
                <xsl:when test="$w/@s='eigennamme'">Pos.PROPN</xsl:when>
                <xsl:when test="$w/@s='haadtw.'">Pos.VERB</xsl:when>
                <xsl:when test="$w/@s='helptw.'">Pos.AUX</xsl:when>
                <xsl:when test="$w/@s='keppeltw.'">Pos.AUX</xsl:when>
                <xsl:when test="$w/@s='interjeksje'">Pos.INTJ</xsl:when>
                <xsl:when test="$w/@s='partikel'">Pos.PART</xsl:when>
                <xsl:when test="$w/@s='pronomen' and not(contains($w/@t,'CONund') or contains($w/@t,'ûndersk'))">Pos.PRON</xsl:when>
                <xsl:when test="$w/@s='numerale'">Pos.NUM</xsl:when>
                <xsl:when test="$w/@s='preposysje'">Pos.ADP</xsl:when>
                <xsl:when test="$w/@s='artikel'">PronType.Art</xsl:when>
                <xsl:when test="$w/@s='ôfkoarting'">abbr.yes</xsl:when>
                <xsl:when test="$w/@s='konj.' and not(contains($w/@t,'[lyksk.]') or contains($w/@t,'[ûndersk.]'))">Pos.CCONJ</xsl:when>
                <xsl:when test="$w/@s='de-subst.'">Pos.NOUN Gender.Com</xsl:when>
                <xsl:when test="$w/@s='it-subst.'">Pos.NOUN <xsl:if test="not($w/@t='mt' or contains($w/@t,'-mt'))">Number.Coll</xsl:if></xsl:when>
                <xsl:when test="$w/@s='pl-subst.'">Pos.NOUN Number.Ptan</xsl:when>
                <xsl:when test="$w/@s='subst.'">Pos.NOUN</xsl:when>
            </xsl:choose>
            <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:if test="$w/@persoan">
            <xsl:choose>
                <xsl:when test="$w/@persoan='akkusatyf'">Case.Acc</xsl:when>
                <xsl:when test="$w/@persoan='datyf'">Case.Dat</xsl:when>
                <xsl:when test="$w/@persoan='genityf'">Case.Gen</xsl:when>
                <xsl:when test="$w/@persoan='gjin'"></xsl:when>
                <xsl:when test="$w/@persoan='nom/acc'"><xsl:message>omitting attribute: <xsl:value-of select="'nom/acc'"/>, atts: <xsl:value-of select="$w/@*"/>, w: <xsl:value-of select="$w"/></xsl:message></xsl:when>
                <xsl:when test="$w/@persoan='nominatyf'">Case.Nom</xsl:when>
                <xsl:when test="$w/@persoan='oblique'">Case.Acc</xsl:when>
                <xsl:when test="$w/@persoan='ynstrumintalis'">Case.Ins</xsl:when>
            </xsl:choose>
            <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:if test="$w/@modus">
            <xsl:choose>
                <xsl:when test="$w/@modus='doetiid'">Tense.Past</xsl:when>
                <xsl:when test="$w/@modus='notiid'">Tense.Pres</xsl:when>
            </xsl:choose>
            <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:if test="$w/@type">
            <xsl:choose>
                <xsl:when test="$w/@type='A_komparatyf'">Pos.ADJ Degree.Cmp</xsl:when>
                <xsl:when test="$w/@type='A_superlatyf'">Pos.ADJ Degree.Sup</xsl:when>
                <xsl:when test="$w/@type='A_ymperatyf'">Pos.VERB Mood.Imp</xsl:when>
                <xsl:when test="$w/@type='N_ferlytsing'">Pos.NOUN Degree.dim</xsl:when>
                <xsl:when test="$w/@type='V_gerundium'">Pos.VERB VerbForm.Ger</xsl:when>
                <xsl:when test="$w/@type='V_mw_fuortsettend'"><xsl:if test="not($w/@namfal='Vmulw_adjektivysk')">Pos.VERB </xsl:if>VerbForm.Part tense.pres</xsl:when>
                <xsl:when test="$w/@type='V_mw_ôfslutend'">Pos.VERB VerbForm.Part Tense.past</xsl:when>
                <xsl:when test="$w/@type='V_optatyf'">Pos.VERB Mood.Sub</xsl:when>
                <xsl:when test="$w/@type='V_yndikatyf'">Pos.VERB Mood.Ind</xsl:when>
                <xsl:when test="$w/@type='V_ynfinityf'">Pos.VERB VerbForm.Inf</xsl:when>
            </xsl:choose>
            <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:if test="$w/@namfal">
            <xsl:choose>
                <xsl:when test="$w/@namfal='A_attributyf'">Pos.ADJ Construction.Attr</xsl:when>
                <xsl:when test="$w/@namfal='A_bywurdlik'">Pos.ADV Convertedfrom.adj</xsl:when>
                <xsl:when test="$w/@namfal='A_predikatyf'">Pos.ADJ Predicate.yes</xsl:when>
                <xsl:when test="$w/@namfal='A_selstannich'">Pos.NOUN Convertedfrom.adj</xsl:when>
                <xsl:when test="$w/@namfal='Vmulw_adjektivysk'">Pos.ADJ Convertedfrom.part</xsl:when>
            </xsl:choose>
            <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:if test="$w/@wurdsoarte">
            <xsl:choose>
                <xsl:when test="contains($w/@wurdsoarte,'adj.')">Pos.ADJ </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'adv.')">Pos.ADV </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'asf.')">Case.Acc Number.Sing Gender.Fem </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'nsf.')">Case.Nom Number.Sing Gender.Fem </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'dsf.')">Case.Dat Number.Sing Gender.Fem </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'nsm')">Case.Nom Number.Sing Gender.Masc </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'dem.')">PronType.Dem </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'pron.') and not(contains($w/@t,'ûndersk'))">Pos.PRON </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'N.')">Pos.NOUN </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'sing.') or contains($w/@wurdsoarte,'sg')">Number.Sing </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'masc')">Gender.Masc </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'acc.')">Case.Acc </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'PN') and not(contains($w/@t,'ûndersk'))">Pos.PRON </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'V.')">Pos.VERB </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'imp.')">Mood.Imp </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'opt.')">Mood.Sub </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'pl.')">Number.Plur </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'past')">tense.past VerbForm.Part Pos.VERB </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'pers')">PronType.Prs </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'pres')">Tense.Pres </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'prep')">Pos.ADP </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'pret')">Tense.Past </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'ind')">Mood.Indi </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'1')">Person.1 </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'2')">Person.2 </xsl:when>
                <xsl:when test="contains($w/@wurdsoarte,'3')">Person.3 </xsl:when>
            </xsl:choose>
            <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:if test="$w/@slachte">
            <xsl:choose>
                <xsl:when test="$w/@slachte='iental'">Number.Sing</xsl:when>
                <xsl:when test="$w/@slachte='meartal'">Number.Plur</xsl:when>
            </xsl:choose>
            <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:if test="$w/@tal">
            <xsl:choose>
                <xsl:when test="$w/@tal='earste persoan'">Person.1</xsl:when>
                <xsl:when test="$w/@tal='earste'">Person.1</xsl:when>
                <xsl:when test="$w/@tal='tredde persoan'">Person.3</xsl:when>
                <xsl:when test="$w/@tal='tredde'">Person.3</xsl:when>
                <xsl:when test="$w/@tal='twadde'">Person.2</xsl:when>
            </xsl:choose>
            <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:if test="$w/@tiid">
            <xsl:choose>
                <xsl:when test="$w/@tiid='common_gender'">Gender.Neut</xsl:when>
                <xsl:when test="$w/@tiid='froulik'">Gender.Fem</xsl:when>
                <xsl:when test="$w/@tiid='manlik'">Gender.Masc</xsl:when>
                <xsl:when test="$w/@tiid='m/n'">Gender.Neut</xsl:when>
                <xsl:when test="$w/@tiid='ûnsidich'">Gender.Neut</xsl:when>
            </xsl:choose>
            <xsl:text> </xsl:text>
        </xsl:if>
    </xsl:template>

    <!-- logic to convert Lemma.soart and values from Soartlist.soartlabel to universaldependencies -->
    <xsl:template name="convert_t">
        <xsl:param name="t" select="''"/>
        <xsl:if test="$t">
            <xsl:choose>
                <xsl:when test="$t='adjektyf'">Pos.ADJ</xsl:when>
                <xsl:when test="$t='adverbium'">Pos.ADV</xsl:when>
                <xsl:when test="$t='eigennamme'">Pos.PROPN</xsl:when>
                <xsl:when test="$t='haadtw.'">Pos.VERB</xsl:when>
                <xsl:when test="$t='helptw.'">Pos.AUX</xsl:when>
                <xsl:when test="$t='keppeltw.'">Pos.AUX</xsl:when>
                <xsl:when test="$t='interjeksje'">Pos.INTJ</xsl:when>
                <xsl:when test="$t='partikel'">Pos.PART</xsl:when>
                <xsl:when test="$t='pronomen'">Pos.PRON</xsl:when>
                <xsl:when test="$t='polite-nt'">Pos.VERB Person.2 Tense.Pres Polite.Form </xsl:when>
                <xsl:when test="$t='polite-dt'">Pos.VERB Person.2 Tense.Past Polite.Form </xsl:when>
                <xsl:when test="$t='subj'">Pos.PRON PronType.Prs Case.Nom </xsl:when>
                <xsl:when test="$t='obj'">Pos.PRON PronType.Prs Case.Acc </xsl:when>
                <xsl:when test="$t='numerale'">Pos.NUM</xsl:when>
                <xsl:when test="$t='preposysje'">Pos.ADP</xsl:when>
                <xsl:when test="$t='artikel'">PronType.Art Pos.DET</xsl:when>
                <xsl:when test="$t='ôfkoarting'">abbr.yes</xsl:when>
                <xsl:when test="$t='konj.'">Pos.CCONJ</xsl:when>
                <xsl:when test="$t='de-subst.'">Pos.NOUN Gender.Com</xsl:when>
                <xsl:when test="$t='it-subst.'">Pos.NOUN</xsl:when>
                <xsl:when test="$t='pl-subst.'">Pos.NOUN Number.Ptan</xsl:when>
                <xsl:when test="contains($t,'subst')">Pos.NOUN</xsl:when>
                <xsl:otherwise>
                    <xsl:choose>
                        <xsl:when test="contains($t,'1-it-dt')">Person.1 Number.Sing <xsl:if test="not(contains($t,'takom.tiid'))">Tense.Past </xsl:if></xsl:when>
                        <xsl:when test="contains($t,'1-it-nt')">Person.1 Number.Sing <xsl:if test="not(contains($t,'takom.tiid'))">Tense.Pres </xsl:if></xsl:when>
                        <xsl:when test="contains($t,'1-mt-dt')">Person.1 Number.Plur <xsl:if test="not(contains($t,'takom.tiid'))">Tense.Past </xsl:if></xsl:when>
                        <xsl:when test="contains($t,'1-mt-nt')">Person.1 Number.Plur <xsl:if test="not(contains($t,'takom.tiid'))">Tense.Pres </xsl:if></xsl:when>
                        <xsl:when test="contains($t,'2-it-dt')">Person.2 Number.Sing <xsl:if test="not(contains($t,'takom.tiid'))">Tense.Past </xsl:if></xsl:when>
                        <xsl:when test="contains($t,'2-it-nt')">Person.2 Number.Sing <xsl:if test="not(contains($t,'takom.tiid'))">Tense.Pres </xsl:if></xsl:when>
                        <xsl:when test="contains($t,'2-mt-dt')">Person.2 Number.Plur <xsl:if test="not(contains($t,'takom.tiid'))">Tense.Past </xsl:if></xsl:when>
                        <xsl:when test="contains($t,'2-mt-nt')">Person.2 Number.Plur <xsl:if test="not(contains($t,'takom.tiid'))">Tense.Pres </xsl:if></xsl:when>
                        <xsl:when test="contains($t,'3-it-dt')">Person.3 Number.Sing  <xsl:if test="not(contains($t,'takom.tiid'))">Tense.Past </xsl:if></xsl:when>
                        <xsl:when test="contains($t,'3-it-nt')">Person.3 Number.Sing  <xsl:if test="not(contains($t,'takom.tiid'))">Tense.Pres </xsl:if></xsl:when>
                        <xsl:when test="contains($t,'3-mt-dt')">Person.3 Number.Plur <xsl:if test="not(contains($t,'takom.tiid'))">Tense.Past </xsl:if></xsl:when>
                        <xsl:when test="contains($t,'3-mt-nt')">Person.3 Number.Plur <xsl:if test="not(contains($t,'takom.tiid'))">Tense.Pres </xsl:if></xsl:when>
                        <xsl:when test="contains($t,'2-it')">Person.2 Number.Sing </xsl:when>
                        <xsl:when test="contains($t,'-it')">Number.Sing </xsl:when>
                        <xsl:when test="contains($t,'-mt')">Number.Plur </xsl:when>
                        <xsl:when test="$t='mt'">Number.Plur </xsl:when>
                    </xsl:choose>
                    <xsl:if test="matches($t,'^[^k]?om') or matches($t,'[^k]om')">VerbForm.Part Tense.Past </xsl:if>
                    <xsl:if test="contains($t,'fm')">VerbForm.Part Tense.Pres </xsl:if>
                    <xsl:if test="contains($t,'CONund') and not(contains($t,'ûndersk'))">
                        <xsl:message>not coding pos.sconj for: <xsl:value-of select="$t"/></xsl:message>
                    </xsl:if>
                    <xsl:if test="contains($t,'ûndersk')">Pos.SCONJ </xsl:if>
                    <xsl:if test="contains($t,'[lyksk.]')">Pos.CCONJ </xsl:if>
                    <xsl:if test="contains($t,'sels')">Pos.NOUN </xsl:if>
                    <xsl:if test="contains($t,'[passyf]')">Voice.Pass </xsl:if>
                    <xsl:if test="contains($t,'[modaal]')">VerbType.Mod </xsl:if>
                    <xsl:if test="contains($t,'[folsl.tiid]')">VerbType.Tense </xsl:if>
                    <xsl:if test="contains($t,'takom.tiid')">Tense.Fut </xsl:if>
                    <xsl:if test="contains($t,'=nf')">VerbForm.Inf </xsl:if>
                    <xsl:if test="contains($t,'sup')">Degree.Sup </xsl:if>
                    <xsl:if test="contains($t,'komp')">Degree.Cmp </xsl:if>
                    <xsl:if test="contains($t,'rtw')">NumType.Ord </xsl:if>
                    <xsl:if test="contains($t,'htw')">NumType.Card </xsl:if>
                    <xsl:if test="contains($t,'hf')">Mood.Imp </xsl:if>
                    <xsl:if test="contains($t,'df')">VerbForm.Ger </xsl:if>
                    <xsl:if test="contains($t,'bwg')">VerbForm.Conv </xsl:if>
                    <xsl:if test="contains($t,'pgn')">Case.Par </xsl:if>
                    <xsl:if test="contains($t,'gn') and not(contains($t,'pgn'))">Case.Gen </xsl:if>
                    <xsl:if test="contains($t,'emf')">PronType.Emp </xsl:if>
                    <xsl:if test="contains($t,'-in')">PronType.Ind </xsl:if>
                    <xsl:if test="contains($t,'pred')">Predicate.yes </xsl:if>
                    <xsl:choose>
                        <xsl:when test="contains($t,'bytrans')">Valency.3 </xsl:when>
                        <xsl:when test="contains($t,'yntrans')">Valency.1 </xsl:when>
                        <xsl:when test="contains($t,'monotr')">Valency.2 </xsl:when>
                        <xsl:when test="contains($t,'[yntrans./trans]')">Valency.Tran </xsl:when>
                    </xsl:choose>
                    <xsl:if test="contains($t,'nulfoarm.')">Inflection.Uninf islemma.yes </xsl:if>
                    <xsl:choose>
                        <xsl:when test="contains($t,'-PRO')">prodrop.yes </xsl:when>
                        <xsl:when test="contains($t,'?PRO')">clitic.yes </xsl:when>
                    </xsl:choose>
                    <xsl:if test="contains($t,'fw')">Degree.dim </xsl:if>
                    <xsl:choose>
                        <xsl:when test="matches($t,'-b[^w]') or matches($t,'-b$') or matches($t,'^b-')">Inflection.Infl </xsl:when>
                        <xsl:when test="contains($t,'nb-') or contains($t,'-nb')">Inflection.Uninf </xsl:when>
                    </xsl:choose>
                    <xsl:if test="contains($t,'attr')">Construction.Attr </xsl:if>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

</xsl:stylesheet>

