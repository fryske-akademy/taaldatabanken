<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="2.0 "
                xmlns:fa="http://frisian.eu/tei-ud-linguistics" xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
    <xsl:include href="../resources/TEI/stylesheet/html/html.xsl"/>
    <!--    <xsl:param name="logoFile">logo.png</xsl:param>
    <xsl:param name="logoWidth">60</xsl:param>
    <xsl:param name="logoHeight">60</xsl:param>
    <xsl:param name="cssFile">myTEI.css </xsl:param>
    <xsl:param name="pageLayout">Complex</xsl:param>-->
    <xsl:param name="outputMethod">xml</xsl:param>
    <xsl:param name="parentWords">The Frisian Taaldatabank Project</xsl:param>
    <xsl:param name="institution">Fryske Akademy</xsl:param>
    
    <xsl:param name="schemaFile" select="'file:/home/eduard/linguistic-customization/src/main/resources/xsd/grammar.xsd'"/>
    <xsl:variable name="schema" select="doc($schemaFile)"/>
    
    <xsl:template match="w">
        <xsl:variable name="lemma">
            <xsl:choose>
                <xsl:when test="@lemma">
                    <xsl:value-of select="@lemma"/>
                </xsl:when>
                <xsl:when test="following-sibling::join[1][contains(@target,current()/@xml:id)]">
                    <xsl:value-of
                        select="following-sibling::join[1][contains(@target,current()/@xml:id)]/@lemma"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="lemmaRef">
            <xsl:choose>
                <xsl:when test="@lemmaRef">
                    <xsl:value-of select="@lemmaRef"/>
                </xsl:when>
                <xsl:when test="following-sibling::join[1][contains(@target,current()/@xml:id)]">
                    <xsl:value-of
                        select="following-sibling::join[1][contains(@target,current()/@xml:id)]/@lemmaRef"/>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="ana">
            <xsl:choose>
                <xsl:when test="@fa:*">
                    <xsl:for-each select="@fa:*">
                        <xsl:value-of select="local-name()"/>
                        <xsl:value-of select="':'"/>
                        <xsl:value-of select="."/>
                        <xsl:value-of select="' '"/>
                    </xsl:for-each>
                </xsl:when>
                <xsl:when test="following-sibling::join[1][contains(@target,current()/@xml:id)]">
                    <xsl:for-each select="following-sibling::join[1][contains(@target,current()/@xml:id)]/@fa:*">
                        <xsl:value-of select="local-name()"/>
                        <xsl:value-of select="':'"/>
                        <xsl:value-of select="."/>
                        <xsl:value-of select="' '"/>
                    </xsl:for-each>
                </xsl:when>
            </xsl:choose>
        </xsl:variable>
        <span>
            <xsl:if test="@next or @prev">
                <xsl:attribute name="id" select="@xml:id"/>
                <xsl:attribute name="onmouseover">
                    showParts(this,'<xsl:value-of select="if (@next) then substring(@next,2) else substring(@prev,2)"/>',true);
                </xsl:attribute>
                <xsl:attribute name="onmouseout">
                    showParts(this,'<xsl:value-of select="if (@next) then substring(@next,2) else substring(@prev,2)"/>',false);
                </xsl:attribute>
            </xsl:if>
            <xsl:attribute 
                name="title" select="concat('(lemma: ',$lemma,') ', $ana)"/>
            <xsl:choose>
                <xsl:when test="$lemmaRef!=''">
                    <a href="{$lemmaRef}">
                        <xsl:apply-templates/>
                    </a>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates/>
                </xsl:otherwise>
            </xsl:choose>
        </span>
    </xsl:template>
    
    <xsl:template match="join"/>

    <xsl:template match="body">
        <xsl:variable name="anas">
            <xsl:value-of select="distinct-values(//w/@fa:*|//join/@fa:*)"/>
        </xsl:variable>
        <form>
            <script type="text/javascript">
                <xsl:text disable-output-escaping="yes">
                    <![CDATA[
                var filter, first, gotoFirst, word;
                function showParts(e,id,highlight) {
                    var c = (highlight)?'lightgreen':'white';
                    document.getElementById(id).setAttribute('style','background-color:' + c);
                    e.setAttribute('style','background-color:' + c);
                }
                function highlightElements() {
                    filter.innerHTML = 'current filter: ' + (word.value==''?'':'containing: '+word.value) +
                        (not1.checked ? ' not ' : '') + select1.value + 
                        (andOr1.value == 'and' ? ' and ' : ' or ') + (not2.checked ? ' not ' : '') + select2.value +
                        (andOr2.value == 'and' ? ' and ' : ' or ') + (not3.checked ? ' not ' : '') + select3.value +
                        (andOr3.value == 'and' ? ' and ' : ' or ') + (not4.checked ? ' not ' : '') + select4.value +
                        (andOr4.value == 'and' ? ' and ' : ' or ') + (not5.checked ? ' not ' : '') + select5.value + ' (or terms are parenthesized)';
                    var allElements = document.getElementsByTagName('span');
                    var ti = 0;
                    first=false; gotoFirst.style.display='none';
                    var evt = function() { this.style.fontWeight='bold'; };
                    var out = function() { this.style.fontWeight=''; };
                    var m = 0, tot = 0;
                    for (var i = 0; i < allElements.length; i++) {
                        tot++;
                        if (allElements[i].hasAttribute('title')) {
                            var e = allElements[i];
                            var a = e.getElementsByTagName('a')[0];
                            if (matchesFilter(e,a)) {
                                m++;
                                if (a) {
                                    a.tabIndex=++ti;
                                    a.style.backgroundColor='orange';
                                    a.addEventListener('focus', evt);
                                    a.addEventListener('blur', out);
                                    if (!first) first = a;
                                } else {
                                    e.style.backgroundColor='orange';
                                }
                            } else {
                                if (a) {
                                    a.tabIndex=-1;
                                    a.removeAttribute('style');
                                    a.removeEventListener('focus',evt);
                                } else {
                                    e.removeAttribute('style');
                                }
                            }

                        } 
                    }
                    filter.innerHTML += '. ' + m + ' match in ' + tot + ' total words.';
                    if (first) gotoFirst.style.display='';
                }
                function matchesFilter(e,anchor) {
                    var txt = (anchor) ? anchor.innerHTML : e.innerHTML;
                    var a = e.getAttribute('title');
                    var matchText = word.value=='' || txt.search(word.value) > -1;
                    if (select1.value=='-'&&select2.value=='-'&&select3.value=='-'&&select4.value=='-'&&select5.value=='-') {
                        return word.value!='' && matchText;
                    }
                    if (!matchText) return false;
                    var s1 = (select1.value == '-' || not1.checked ? a.indexOf(select1.value) == -1 : a.indexOf(select1.value) > -1);
                    var s2 = (select2.value == '-' || not2.checked ? a.indexOf(select2.value) == -1 : a.indexOf(select2.value) > -1);
                    var s3 = (select3.value == '-' || not3.checked ? a.indexOf(select3.value) == -1 : a.indexOf(select3.value) > -1);
                    var s4 = (select4.value == '-' || not4.checked ? a.indexOf(select4.value) == -1 : a.indexOf(select4.value) > -1);
                    var s5 = (select5.value == '-' || not5.checked ? a.indexOf(select5.value) == -1 : a.indexOf(select5.value) > -1);
                    var s1o = (select1.value != '-' && not1.checked ? a.indexOf(select1.value) == -1 : a.indexOf(select1.value) > -1);
                    var s2o = (select2.value != '-' && not2.checked ? a.indexOf(select2.value) == -1 : a.indexOf(select2.value) > -1);
                    var s3o = (select3.value != '-' && not3.checked ? a.indexOf(select3.value) == -1 : a.indexOf(select3.value) > -1);
                    var s4o = (select4.value != '-' && not4.checked ? a.indexOf(select4.value) == -1 : a.indexOf(select4.value) > -1);
                    var s5o = (select5.value != '-' && not5.checked ? a.indexOf(select5.value) == -1 : a.indexOf(select5.value) > -1);
                    var a1 = andOr1.value == 'and';
                    var a2 = andOr2.value == 'and';
                    var a3 = andOr3.value == 'and';
                    var a4 = andOr4.value == 'and';
                    if (a1) {
                        if (a2) {
                            if (a3) {
                                if (a4) {
                                    return s1 && s2 && s3 && s4 && s5;
                                } else {
                                    return s1 && s2 && s3 && ( s4o || s5o );
                                }
                            } else {
                                if (a4) {
                                    return s1 && s2 && (s3o || s4o) && s5;
                                } else {
                                    return s1 && s2 && (s3o || s4o || s5o );
                                }
                            }
                        } else {
                            if (a3) {
                                if (a4) {
                                    return s1 && (s2o || s3o) && s4 && s5;
                                } else {
                                    return s1 && (s2o || s3o) && ( s4o || s5o );
                                }
                            } else {
                                if (a4) {
                                    return s1 && (s2o || s3o || s4o) && s5;
                                } else {
                                    return s1 && (s2o || s3o || s4o || s5o );
                                }
                            }
                        }
                    } else {
                        if (a2) {
                            if (a3) {
                                if (a4) {
                                    return (s1o || s2o) && s3 && s4 && s5;
                                } else {
                                    return (s1o || s2o) && s3 && ( s4o || s5o );
                                }
                            } else {
                                if (a4) {
                                    return (s1o || s2o) && (s3o || s4o) && s5;
                                } else {
                                    return (s1o || s2o) && (s3o || s4o || s5o );
                                }
                            }
                        } else {
                            if (a3) {
                                if (a4) {
                                    return (s1o || s2o || s3o) && s4 && s5;
                                } else {
                                    return (s1o || s2o || s3o) && ( s4o || s5o );
                                }
                            } else {
                                if (a4) {
                                    return (s1o || s2o || s3o || s4o) && s5;
                                } else {
                                    return (s1o || s2o || s3o || s4o || s5o );
                                }
                            }
                        }
                    }
                }
                function focusFirst() {first.focus();}
                ]]></xsl:text>
            </script>
            <br/>
            <br/>
            search for words with combinations of lexical properties (use TAB to jump to next highlighted word):<br/>
            <br/>
            <xsl:variable name="options">
                <xsl:for-each select="$schema/xs:schema/xs:attribute/xs:simpleType/xs:restriction/xs:enumeration[contains($anas,@value)]">
                    <option title="{xs:annotation/xs:documentation}" value="{ancestor::xs:attribute/@name}:{@value}">
                        <xsl:value-of select="concat(ancestor::xs:attribute/@name,':',@value)"/>
                    </option>
                </xsl:for-each>            
            </xsl:variable>
            search text <input type="text" id="word" title="regular expression to search"/> (regex)<br/>
            <xsl:call-template name="select">
                <xsl:with-param name="options" select="$options"/>
            </xsl:call-template>
            <input type="button" id="first" value="goto first match" style="display: none" onclick="focusFirst();"/>
            <br/>
            <input type="button" value="search" onclick="highlightElements();"/>
            <span id="filter"></span>
            <br/>
            <script type="text/javascript">
                <xsl:text disable-output-escaping="yes">
                    <![CDATA[
                        word = document.getElementById('word');
                        gotoFirst = document.getElementById('first');
                        filter = document.getElementById('filter');
                    ]]>
                </xsl:text>
            </script>
        </form>
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template name="select">
        <xsl:param name="options"/>
        <xsl:for-each select="1 to 5">
            <label for="{concat('not',.)}" style="font-wheight: bold; font-size: 150%">!</label><input type="checkbox" id="{concat('not',.)}" title="checked means not"/>
            <select id="{concat('select',.)}">
                <option title="choose" value="-"></option>
                <xsl:copy-of select="$options"/>
            </select>
            <xsl:if test=". &lt; 5">
                &#160;<select id="{concat('andOr',.)}">
                    <option value="and">and</option>
                    <option value="or">or</option>
                </select>&#160;
            </xsl:if>
            <script type="text/javascript">
                var not<xsl:value-of select="."/> = document.getElementById('not<xsl:value-of select="."/>');
                var select<xsl:value-of select="."/> = document.getElementById('select<xsl:value-of select="."/>');
                <xsl:if test=". &lt; 5">
                var andOr<xsl:value-of select="."/> = document.getElementById('andOr<xsl:value-of select="."/>');
                </xsl:if>
            </script>
        </xsl:for-each>
    </xsl:template>
     
</xsl:stylesheet>
