import net.sf.saxon.Configuration;
import net.sf.saxon.lib.DirectResourceResolver;
import net.sf.saxon.lib.ResourceRequest;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XdmNode;
import net.sf.saxon.s9api.XsltCompiler;
import net.sf.saxon.s9api.XsltExecutable;
import net.sf.saxon.trans.XPathException;
import org.fa.tdb.conversie.Conversie;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;

import javax.xml.transform.Source;
import javax.xml.transform.SourceLocator;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import net.sf.saxon.s9api.MessageListener;

public class ConversieTest {

    public static final Logger LOGGER = LoggerFactory.getLogger(ConversieTest.class);
    @Test
    public void testConversie() throws SaxonApiException, IOException {
        final MessageListener messageListener = new MessageListener() {
            @Override
            public void message(XdmNode xn, boolean bln, SourceLocator sl) {
                LOGGER.info(xn.toString());
            }
        };
        Processor processor = new Processor(Configuration.newConfiguration());
        final DirectResourceResolver directResourceResolver = new DirectResourceResolver(processor.getUnderlyingConfiguration()) {
            @Override
            public Source resolve(ResourceRequest request) throws XPathException {
                String uri = request.uri.toString();
                if (uri.startsWith("classpath:")) {
                    InputStreamReader reader = new InputStreamReader(
                            Conversie.class.getResourceAsStream(uri.toString().substring(10)));
                    return new SAXSource(new InputSource(reader));
                } else {
                    return super.resolve(request);
                }
            }
        };
        XsltCompiler xsltCompiler = processor.newXsltCompiler();
        InputStream xslt = Conversie.class.getResourceAsStream("/xslt/convert-ynt-tdb.xslt");
        XsltExecutable conversieSheet = xsltCompiler.compile(new StreamSource(xslt));

        Conversie.convert(Path.of("src/test/resources/gj5030.xml"),
                Path.of("/tmp/weg.xml"),
                processor,
                conversieSheet,messageListener,directResourceResolver
        );

        xslt = Conversie.class.getResourceAsStream("/xslt/convert-leks-tdb.xslt");
        conversieSheet = xsltCompiler.compile(new StreamSource(xslt));

        Conversie.convert(Path.of("src/test/resources/barbus01.xml"),
                Path.of("/tmp/weg.xml"),
                processor,
                conversieSheet,messageListener,directResourceResolver
        );

    }
}
