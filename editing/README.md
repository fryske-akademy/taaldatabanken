## annotations.odd

```xml
    <elementSpec ident="w" mode="change">
        <model predicate="@lemma" behaviour="inline" cssClass="annotation annotation-lemma"/>
    </elementSpec>
```
## annotate.html

```xml
    <paper-icon-button class="annotation-action" title="Lemma" data-type="lemma" icon="icons:label" disabled="disabled"/>
```
```xml
    <div class="annotation-form lemma">
        <paper-input class="annotation-form lemma" name="lemma" label="Lemma"/>
    </div>
```

## annotation-config.xqm

```xquery
    case "lemma"
        return if ($content() instance of xs:string) then (attribute lemma {$properties?lemma}, $content()) else $content()
.
.
    case "lemma" return
        collection($config:data-default)//tei:w[. = $key]
```