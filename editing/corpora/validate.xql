module namespace trigger="http://exist-db.org/xquery/trigger";

declare namespace validation="http://exist-db.org/xquery/validation";

declare function trigger:before-create-collection($uri as xs:anyURI) {util:log('warn','before-create-collection')};

declare function trigger:after-create-collection($uri as xs:anyURI) {util:log('warn','after-create-collection')};

declare function trigger:before-copy-collection($uri as xs:anyURI, $new-uri as xs:anyURI) {util:log('warn','before-copy-collection')};

declare function trigger:after-copy-collection($new-uri as xs:anyURI, $uri as xs:anyURI) {util:log('warn','after-copy-collection')};

declare function trigger:before-move-collection($uri as xs:anyURI, $new-uri as xs:anyURI) {util:log('warn','before-move-collection')};

declare function trigger:after-move-collection($new-uri as xs:anyURI, $uri as xs:anyURI) {util:log('warn','after-move-collection')};

declare function trigger:before-delete-collection($uri as xs:anyURI) {util:log('warn','before-delete-collection')};

declare function trigger:after-delete-collection($uri as xs:anyURI) {util:log('warn','after-delete-collection')};

declare function trigger:before-create-document($uri as xs:anyURI) {util:log('warn','before-create-document')};

declare function trigger:after-create-document($uri as xs:anyURI) {util:log('warn','after-create-document')};

declare function trigger:before-update-document($uri as xs:anyURI) {util:log('warn','before-update-document')};

declare function trigger:after-update-document($uri as xs:anyURI) {util:log('warn','after-update-document')};

declare function trigger:before-copy-document($uri as xs:anyURI, $new-uri as xs:anyURI) {util:log('warn','before-copy-document')};

declare function trigger:after-copy-document($new-uri as xs:anyURI, $uri as xs:anyURI) {util:log('warn','after-copy-document')};

declare function trigger:before-move-document($uri as xs:anyURI, $new-uri as xs:anyURI) {util:log('warn','before-move-document')};

declare function trigger:after-move-document($new-uri as xs:anyURI, $uri as xs:anyURI) {util:log('warn','after-move-document')};

declare function trigger:before-delete-document($uri as xs:anyURI) {util:log('warn','before-delete-document')};

declare function trigger:after-delete-document($uri as xs:anyURI) {util:log('warn','after-delete-document')};

declare function local:validate($type as xs:string, $event as xs:string, $object-type as xs:string, $uri as xs:string) {
    let $report := validation:jing-report(xs:anyURI($uri), doc("xmldb:exist:///db/apps/corpora/schematron/corpora_linguistics.sch"))
    return if ($report/status='invalid') then
        (
            util:log('warn',$report/message),
            error(QName('http://corpora.fa','invalid'))
        )
    else ()
};