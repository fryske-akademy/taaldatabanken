xquery version "3.0";

(: The following external variables are set by the repo:deploy function :)

(: file path pointing to the exist installation directory :)
declare variable $home external;
(: path to the directory containing the unpacked .xar package :)
declare variable $dir external;
(: the target collection into which the app is deployed :)
declare variable $target external;

(sm:chmod(xs:anyURI($target || "/modules/view.xql"), "rwxr-Sr-x"),
sm:chmod(xs:anyURI($target || "/modules/viewXml.xql"), "rwxr-Sr-x"),
xmldb:copy-resource($target,"collection.html","/db/apps/tei-publisher/data","collection.html"),
xmldb:copy-resource($target,"fryske-akademy.odd","/db/apps/tei-publisher/odd","fryske-akademy.odd"),
sm:chgrp(xs:anyURI("/db/apps/tei-publisher/odd/fryske-akademy.odd"),'tei'),
sm:chown(xs:anyURI("/db/apps/tei-publisher/odd/fryske-akademy.odd"),'tei-demo')
(: TODO copying works fine, now somehow trigger recompile :)
)