<xsl:transform xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.tei-c.org/ns/1.0" xmlns:fa="http://frisian.eu/tei-ud-linguistics"
               version="3.0">

    <xsl:template match="*[@sent_id]">
        <s xml:id="_{@sent_id}">
            <xsl:for-each select="*">
                <xsl:choose>
                    <xsl:when test="@upostag!='PUNCT'">
                        <xsl:variable name="idx" select="position()"/>
<!-- we skip conllu lines belonging to a "group" -->
                        <xsl:if test="not(../*[(position()=$idx - 1 or position()=$idx - 2) and contains(@id,'-')])">
                            <xsl:call-template name="doWord">
<!-- if we encounter a group process all lines belonging to it                               -->
                                <xsl:with-param name="words"
                                                select="if (contains(@id,'-')) then .|following-sibling::*[1 or 2] else ."/>
                            </xsl:call-template>
                        </xsl:if>
                    </xsl:when>
                    <xsl:otherwise>
                        <pc xml:id="_{../@sent_id}_{@id}">
                            <xsl:value-of select="@form"/>
                        </pc>
                    </xsl:otherwise>
                </xsl:choose>

                <xsl:if test="@misc=''">
                    <xsl:text> </xsl:text>
                </xsl:if>
                <xsl:value-of select="translate(substring-after(@misc,'SpacesAfter='),'nst\\','&#10; &#9;')"/>
            </xsl:for-each>
            <xsl:if test="*[@deprel]">
                <linkGrp type="UD-SYN" targFunc="head argument">
                    <xsl:for-each select="*[@deprel!='']">
                        <link ana="{@deprel}" target="#_{../@sent_id}_{@head} #_{../@sent_id}_{@id}">
                            <xsl:if test="@deprel='compound:prt'">
                                <xsl:variable name="ww" select="."/>
                                <xsl:attribute name="lemma" select="concat($ww/@lemma,../_[@id=$ww/@head]/@lemma)"/>
                            </xsl:if>
                        </link>
                    </xsl:for-each>
                </linkGrp>
            </xsl:if>
        </s>
    </xsl:template>

    <xsl:template name="w-or-m">
        <xsl:param name="el"/>
        <xsl:attribute name="lemma" select="$el/@lemma"/>
        <xsl:attribute name="pos" select="lower-case($el/@upostag)"/>
        <xsl:call-template name="feature">
            <xsl:with-param name="p" select="concat(lower-case($el/@feats),'|')"/>
        </xsl:call-template>
        <xsl:value-of select="$el/@form"/>
    </xsl:template>

    <xsl:template name="doWord">
<!-- words is an array containing 1 (possibly prodrop) or 3 (for clitic) elements       -->
        <xsl:param name="words"/>
        <w xml:id="_{../@sent_id}_{@id}">
            <xsl:choose>
                <xsl:when test="contains(lower-case(@feats),'prodrop')">
                    <m type="root">
                        <xsl:call-template name="w-or-m"><xsl:with-param name="el" select="."/></xsl:call-template>
                    </m>
                    <m lemma="do" pos="pron" fa:person="2" fa:prontype="prs" fa:prodrop="yes"/>
                </xsl:when>
                <xsl:when test="count($words)=1">
                    <xsl:call-template name="w-or-m"><xsl:with-param name="el" select="."/></xsl:call-template>
                </xsl:when>
                <xsl:otherwise>
                    <m type="root" xml:id="_{../@sent_id}_{$words[2]/@id}">
                        <xsl:call-template name="w-or-m"><xsl:with-param name="el" select="$words[2]"/></xsl:call-template>
                    </m>
                    <m xml:id="_{../@sent_id}_{$words[3]/@id}">
                        <xsl:call-template name="w-or-m"><xsl:with-param name="el" select="$words[3]"/></xsl:call-template>
                    </m>
                </xsl:otherwise>
            </xsl:choose>
        </w>
    </xsl:template>

    <xsl:template name="feature">
        <xsl:param name="p"/>
        <xsl:if test="contains($p,'=')">
            <xsl:variable name="name" select="substring-before($p,'=')"/>
            <xsl:variable name="value" select="substring-after(substring-before($p,'|'),'=')"/>
            <xsl:choose>
                <xsl:when test="$name='definite'"/>
                <xsl:when test="$name='degree' and $value='pos'"/>
                <xsl:when test="$name='gender' and $value='com,neut'"/>
                <xsl:when test="$name='verbform' and $value='fin'"/>
                <xsl:otherwise>
                    <xsl:attribute name="fa:{$name}" select="$value"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:call-template name="feature">
                <xsl:with-param name="p" select="substring-after($p,'|')"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
</xsl:transform>
