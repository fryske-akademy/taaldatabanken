#!/usr/bin/env python3

import sys
from datetime import datetime

import lxml.etree
from lxml import isoschematron
from lxml.isoschematron import Schematron
from lxml.etree import parse, Element, ElementTree, XMLSchema
from openpyxl.xml.constants import XML_NS
from ufal.udpipe import Model, Sentence, Word


model = Model.load(sys.argv[1])

XNS = "{%s}" % XML_NS

FAL = "{%s}" % "http://frisian.eu/tei-ud-linguistics"

T = "http://www.tei-c.org/ns/1.0"

TEI = "{%s}" % T

dt = str(datetime.now()).replace(" ","T")

# rng: RelaxNG = lxml.etree.RelaxNG(file="corpora_linguistics.rng") TODO this hangs!
xmlschema_doc = lxml.etree.parse("corpora_linguistics.xsd")
xmlschema: XMLSchema = lxml.etree.XMLSchema(xmlschema_doc)
# TODO no xslt2 support WHY!!!!
# schematron_doc = lxml.etree.parse("corpora_linguistics.sch")
# schematron: Schematron = isoschematron.Schematron(schematron_doc)

print("start tagging " + dt + (", overwriting" if (sys.argv[2]=="overwrite") else ", writing to *.conv.xml"))

def posLemFeat(el: Element, wordu: Word):
    el.set("pos", wordu.upostag.lower())
    el.set("lemma", wordu.lemma)
    for k,v in [f.split("=") for f in wordu.feats.lower().split("|")
                if "=" in f and not(f.startswith("definite") or
                        f=="degree=pos" or
                        f=="verbform=fin" or
                        f=="gender=com,neut")]:
        el.set(FAL + k, v)

def doM(wordx: Element,wordu: Word,txt: str,root: bool=False, id: bool=False):
    m:Element=Element(TEI+"m")
    if root: m.set("type","root")
    if id: m.set(XNS+"id","UD_"+str(i)+"_"+str(wordu.id))
    m.text=txt
    wordx.text=""
    posLemFeat(m,wordu)
    wordx.insert(0 if (root) else 1,m)

def doWord(wordx: Element, wordus):
    if (len(wordus)==1):
        wordu: Word=wordus[0]
        wordx.set(XNS + "id", "UD_" + str(i) + "_" + str(wordu.id))
        if "prodrop" in wordu.feats.lower():
            doM(wordx,wordu,wordx.txt,True)
            m:Element=Element(TEI+"m")
            m.set("lemma","do")
            m.set("pos","pron")
            m.set(FAL+"prontype","prs")
            m.set(FAL+"prodrop","yes")
            wordx.insert(1,m)
        else:
            posLemFeat(wordx,wordu)
    else:
        wordx.set(XNS + "id", "UD_" + str(i) + "_" + str(wordus[1].id))
        doM(wordx,wordus[1],wordus[1].form,True, True)
        doM(wordx,wordus[2],wordus[2].form,False, True)

def teiLink(wordu:Word):
    l:Element=Element(TEI+"link")
    l.set("ana",wordu.deprel)
    l.set("target","#UD_"+str(i)+"_"+str(wordu.head)+" #UD_"+str(i)+"_"+str(wordu.id))
    if wordu.deprel== "compound:prt":
        lp=([wx,wu] for wx, wu in zip(ws, sentence.words[1:]) if wu.id==wordu.head)
        for wx, wu in lp:
            l.set("lemma",wordu.lemma+wu.lemma)
            wordx.set("next" if wordu.head > wordu.id else "prev","#"+"UD_" + str(i) + "_" + str(wordu.head))
            wx.set("prev" if wordu.head > wordu.id else "next","#"+"UD_" + str(i) + "_" + str(wordu.id))
            break
    lgrp.append(l)

def doLink(wordus:[Word]):
    if len(wordus)==1:
        teiLink(wordus[0])
    else:
        teiLink(wordus[1])
        teiLink(wordus[2])

def inGroup(j: int) -> bool:
    return "-" in sentence.words[j-1].form or "-" in sentence.words[j-2].form if (j > 2) else "-" in sentence.words[j-1].form if (j>1) else False

for filename in sys.argv[3:]:
    doc: ElementTree = parse(filename)
    print("processing " + filename)
    for j in doc.findall(".//"+TEI+"linkGrp[@type='UD-SYN']"):
        print (filename+" may have been tagged already.")
        sys.exit(1)

    for tt in doc.iter(TEI+"TEI"):
        tt.set(FAL+"linguisticsversion","4")
        break
    i: int=0
    for s in doc.iter(TEI+"s"):
        i+=1
        ws: [Element] = [w for w in s if (w.text and w.tag==TEI+"w")]
        sentence: Sentence = Sentence()
        for wordx in ws:
            sentence.addWord(wordx.text)
        model.tag(sentence, "")
        model.parse(sentence, "")

        # sentence.words[0] is de root

        iu = 1; # index of sentence.words, 0 is root we don't use
        for wordx in ws:
            if "-" in str(sentence.words[iu].id): # a group, process the three lines belonging to it
                doWord(wordx,[sentence.words[j] for j in [iu,iu+1,iu+2]])
            else: # skip already processed in group
                if not inGroup(iu): doWord(wordx,[sentence.words[iu]])
            iu+=1
        if len(ws)>1:
            lgrp: Element=Element(TEI+"linkGrp")
            lgrp.set("type","UD-SYN")
            lgrp.set("targFunc","head argument")
            iu=1
            for wordx in ws:
                if "-" in str(sentence.words[iu].id):
                    doLink([sentence.words[j] for j in [iu,iu+1,iu+2]])
                else:
                    if not inGroup(iu): doLink([sentence.words[iu]])
                iu+=1
            s.insert(s.index(wordx)+1,lgrp)

    items = doc.find(".//tei:revisionDesc/tei:list[1]",{"tei":T})

    if (items != None):
        it: Element=Element(TEI+"item")
        it.text="Tagged with udpipe ("+sys.argv[1]+"): "
        d:Element=Element(TEI+"date")
        d.set("when",dt)
        it.append(d)

        items.append(it)

    suffix: str = ".conv.xml" if (sys.argv[2]!="overwrite") else ""
    if not xmlschema.validate(doc):
        print(xmlschema.error_log)
        sys.exit(1)
    # schematron.validate(doc) TODO no xslt2
    doc.write(filename+suffix, encoding="utf-8", xml_declaration=True)

print("end tagging "+str(datetime.now()).replace(" ","T"))