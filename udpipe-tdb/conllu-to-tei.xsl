<xsl:transform xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="3.0">

  <xsl:param name="collection-uri"/>
  <xsl:param name="template" select="'tei-template.xml'"/>

  <xsl:variable name="tei" select="doc($template)"/>

  <xsl:include href="process-template.xsl"/>

  <xsl:template match="/">
    <xsl:for-each select="uri-collection($collection-uri)">
      <xsl:result-document href="{replace(.,'\.conllu$','')}.xml">
        <xsl:apply-templates select="$tei/*">
          <xsl:with-param name="lines" select="unparsed-text-lines(.)"/>
        </xsl:apply-templates>
      </xsl:result-document>
    </xsl:for-each>
  </xsl:template>
</xsl:transform>
