# Demonstration: from Frisian text to Blacklab application
In this demonstration you can transform Frisian text into a running corpus linguistics application that helps linguists analyze the text. [run-demo.sh <file|text>](run-demo.sh) performs all necessary steps, your system needs some setup though.  

The script produces two intermediate files and a log file: `test.conllu`, `test.xml` and `log`.  

NOTE: this demo uses Frisian, but the solution is language independent

NOTE: This demo is merely a starting point that can be used as reference to develop a production ready application.

## Setup
- linux with docker, bash, curl, git, jdk 17 and SaxonHE12-5J
- `git clone https://fryske-akademy@bitbucket.org/fryske-akademy/taaldatabanken.git`
- `cd taaldatabanken/udpipe-tdb/blacklab-demo/docker; ./build.sh`
## Running
- `cd taaldatabanken/udpipe-tdb/blacklab-demo`
- `./run.sh "Dit is in Fryske sin."`, of
- `./run.sh frysk.txt`
- follow instructions in the terminal