#!/bin/bash
#
# curl to udpipe service
# xslt to transform to tei
# java IndexTool to build index
# docker to run blacklab
#
export JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64/
export PATH=$JAVA_HOME:$PATH

trap "echo Failure, log:; cat log; exit 1" ERR

echo "annotating text....."
if [ -f $1 ]
then
  curl -f -X POST -H 'Content-Type: text/plain' --data-binary "@$1" https://frisian.eu/udpipeservice/udpipe/process/conllu > test.conllu 2> log
else
  curl -f -X POST -H 'Content-Type: text/plain' --data-binary "$1" https://frisian.eu/udpipeservice/udpipe/process/conllu > test.conllu 2> log
fi

echo "converting to TEI....."
java -cp ~/SaxonHE12-5J/saxon-he-12.5.jar net.sf.saxon.Transform ../conllu-to-tei-streaming.xsl ../conllu-to-tei-streaming.xsl conllu=`pwd`/test.conllu > test.xml

echo "building Blacklab index....."
rm -rf modern-frisian
mkdir modern-frisian
java -cp ~/BlackLab/tools/target/blacklab-tools-4.0.0-SNAPSHOT.jar:~/BlackLab/tools/target/lib nl.inl.blacklab.tools.IndexTool --format-dir . create modern-frisian/ "test.xml" modern-frisian >> log 2>>log

echo "(re)starting) Blacklab....."
p=`hostname -I|grep -o '^[^ ]\{1,\}'`
sed -i "s/\/\/.*:8084/\/\/$p:8084/" blacklab/corpus-frontend.properties
[ -z "$(docker stack ps ud-blacklab-demo -q 2>/dev/null)" ] || docker stack rm ud-blacklab-demo >>log 2>>log
until [ -z "$(docker stack ps ud-blacklab-demo -q 2>/dev/null)" ]; do sleep 1; done
docker stack deploy -c docker-compose.yml ud-blacklab-demo >> log 2>>log

docker service logs -f ud-blacklab-demo_ud-blacklab-demo 2>&1 | if grep -q JMX; then echo "done."; else echo "failure."; fi
echo "Logging in file \"log\". Browse to http://localhost:port/corpus-frontend, port can be found below"
docker service ls -f name=ud-blacklab-demo_ud-blacklab-demo
