<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="3.1"
                xmlns:fa="http://frisian.eu/tei-ud-linguistics"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml">
    <xsl:include href="Stylesheets-7.56.0/html/html.xsl"/>
    <xsl:param name="parentWords">The Frisian Taaldatabank Project</xsl:param>
    <xsl:param name="iiif"/>
    <xsl:variable name="apos">'</xsl:variable>
    <xsl:variable name="doubleapos">''</xsl:variable>

    <xsl:template match="w">
        <xsl:variable name="lemma" select="@lemma"/>
        <xsl:variable name="ana">
            <xsl:for-each select="@fa:*|@pos">
                <xsl:value-of select="local-name()"/>
                <xsl:value-of select="':'"/>
                <xsl:value-of select="."/>
                <xsl:value-of select="' '"/>
            </xsl:for-each>
        </xsl:variable>
        <span>
            <xsl:attribute name="id"><xsl:value-of select="@xml:id"/></xsl:attribute>
            <xsl:if test="name(..)='hl'">
                <xsl:attribute name="class">hl</xsl:attribute>
            </xsl:if>
            <!--            <xsl:attribute name="onmouseover">
                            findSuper('<xsl:value-of select="replace(string-join(text()),$apos,$doubleapos)"/>',
                            '<xsl:value-of select="head(ancestor-or-self::*[@xml:lang]/@xml:lang)"/>');
                        </xsl:attribute>
                        <xsl:attribute name="onmouseout">
                            $('#superl').hide();
                        </xsl:attribute>-->
            <xsl:attribute name="title"><xsl:value-of select="concat('(lemma: ',$lemma,') ', $ana)"/></xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="m[following-sibling::m[@fa:clitic]]">
        <span><xsl:value-of select="concat(.,following-sibling::m)"/></span>
    </xsl:template>

    <xsl:template match="m[not(@fa:prodrop)]" priority="-1"/>

    <xsl:template match="linkGrp">
        <!--        TODO show syntactic relations in a block -->
<!--        <div class="ud-syn">syntactic relations:-->
<!--            <xsl:for-each select="link">-->
<!--                <span>-->
<!--                    <xsl:variable name="ids" select="tokenize(@target,'\s+')"/>-->
<!--                    <xsl:attribute name="onmouseover">-->
<!--                        showParts('<xsl:value-of select="$ids[1]"/>'.substring(1),'<xsl:value-of select="$ids[2]"/>'.substring(1))-->
<!--                    </xsl:attribute>-->
<!--                    <xsl:attribute name="onmouseout">-->
<!--                        hideParts('<xsl:value-of select="$ids[1]"/>'.substring(1),'<xsl:value-of select="$ids[2]"/>'.substring(1))-->
<!--                    </xsl:attribute>-->
<!--                    <xsl:if test="@lemma"><xsl:attribute name="title"><xsl:value-of select="@lemma"/></xsl:attribute></xsl:if>-->
<!--                    <xsl:value-of select="@ana"/>-->
<!--                </span>,-->
<!--            </xsl:for-each>-->
<!--        </div>-->
    </xsl:template>
    <xsl:template match="graphic" priority="1"/>

    <xsl:template name="iiif">
        <xsl:param name="facs"/>
        <xsl:variable name="id" select="'facses'||count(preceding::pb[@facs])"/>
        <div id="loading{$id}" class="facs" style="height: 20px">Loading images....</div>
        <div class="facs" id="{$id}">&#160;</div>
        <!--
        AllowEncodedSlashes NoDecode
        ProxyPass "/corpusiiif/3/" "http://host:8182/iiif/3/" nocanon
        -->
        <script type="text/javascript">
            $(document).ready(async function() {
            if (typeof L === "undefined" || typeof L.tileLayer.iiif !== "function") {
            await new Promise(resolve =<xsl:text disable-output-escaping="yes">></xsl:text> setTimeout(resolve, 2000));
            }
            var map = L.map('<xsl:value-of select="$id"/>', {center: [0, 0], crs: L.CRS.Simple, zoom: 0})
            var ctrl = L.control.layers().addTo(map);
            let l = null;
            let t = null;
            <xsl:for-each select="$facs">
                l = L.tileLayer.iiif('<xsl:value-of select="$iiif"/>/<xsl:value-of select="replace(.,'/','%2F')"/>/info.json');
                if (<xsl:value-of select="position()"/>==1) {
                t=l;
                l.addTo(map);
                }
                ctrl.addBaseLayer(l,'facs <xsl:value-of select="position()"/>');
            </xsl:for-each>
            ctrl.expand();

            t.on('loading', function (event) {
            $('#loading<xsl:value-of select="$id"/>').show();
            });
            t.on('load', function (event) {
            $('#loading<xsl:value-of select="$id"/>').hide();
            t.off('loading');
            t.off('load');
            });
            });
        </script>

    </xsl:template>

    <xsl:template match="pb[@facs]" priority="1">
        <xsl:call-template name="iiif">
            <xsl:with-param name="facs" select="tokenize(@facs)"/>
        </xsl:call-template>
    </xsl:template>

    <xsl:template match="/" priority="1">
        <div>
            <script type="text/javascript">
                $("<link/>", { rel: "stylesheet", type: "text/css",
                href: CONTEXT_URL + "/" + INDEX_ID + "/static/leaflet.css"}).appendTo("head");

                $.getScript(CONTEXT_URL + "/" + INDEX_ID + "/static/leaflet.js").done(function () {
                $.getScript(CONTEXT_URL + "/" + INDEX_ID + "/static/leaflet-iiif.js").done(function () {
                });
                });
            </script>
            <xsl:choose>
                <xsl:when test="//body">
                    <xsl:apply-templates select="//body/*"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="/*/node()"/>
                </xsl:otherwise>
            </xsl:choose>
            <script type="text/javascript">
                <xsl:text disable-output-escaping="yes">
                    <![CDATA[
                function showParts(id1,id2) {
                    document.getElementById(id1).setAttribute('class','bg-danger');
                    document.getElementById(id2).setAttribute('class','bg-danger');
                }
                function hideParts(id1,id2) {
                    document.getElementById(id1).classList.remove('bg-danger');
                    document.getElementById(id2).classList.remove('bg-danger');
                }
                function findSuper(lemma,lang) {
                    langapi.findSuper(lemma,lang=='fry-x-midfrysk'?'fry_x_midfrisian':lang.replace('-','_')).then(function(data) {
                        $('#superl').text(
                            JSON.stringify(data.findsuperlemmas.superlemmas
                            .map(s => ({
                                "pos": s.pos, "meaning": s.meaning, "lemmas":
                                s.lemmas.map(l => l.languagecode + ': ' + l.lemma).join('; ')
                            })).filter(function(v,i,a) {
                                let prev = a[i-1];
                                return prev==undefined||v.pos!=prev.pos||v.meaning!=prev.meaning||v.lemmas!=prev.lemmas
                            })
                            ,null,3)
                        );
                        $('#superl').show();
                    })
                }
                ]]></xsl:text>
            </script>
            <div id="superl" style=" font-size: x-large; position: fixed; top: 50px; left: 50px; z-index: 10; background-color: #cccccc; margin: 15px; display: none"></div>
        </div>
    </xsl:template>


</xsl:stylesheet>
