var scrollUp = function (selector) {
    var c = document.querySelector(selector);
    window.setTimeout(function () {
        $(c).animate({scrollTop: 0}, 500);
    }, 600);
}
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
$(document).ready(function () {
    if (window.vuexModules.corpus) {
        vuexModules.ui.getState().results.shared.getDocumentSummary = function(metadata, specialFields) {
            return metadata.docId ? metadata.docId[0] : 'no id for ' + metadata.fromInputFile[0].replace(/.*(OOR[0-9]*).xml/,"$1");
        }
        var x = true;
        var ui = vuexModules.ui.actions;
        ui.helpers.configureAnnotations([
            [               ,    'EXTENDED'    ,    'ADVANCED'    ,    'EXPLORE'    ,    'SORT'    ,    'GROUP'    ,    'RESULTS'    ,    'CONCORDANCE'    ],

            // Generic aspects
            ['word'         ,        x         ,        x         ,        x        ,      x       ,       x       ,                 ,                     ],
            ['lemma'        ,        x         ,        x         ,        x        ,      x       ,       x       ,        x        ,                     ],
        ]);

        ui.helpers.configureMetadata([
            [                  ,    'FILTER'    ,    'SORT'    ,    'GROUP'    ,    'RESULTS/HITS'    ,    'RESULTS/DOCS'    ,    'EXPORT'    ],

            // Other filters
            ['fromInputFile'   ,                ,              ,               ,                      ,                      ,                ],
            ['title'           ,       x        ,      x       ,       x       ,                      ,          x           ,                ],
            // Main filters
            ['year'            ,       x        ,      x       ,       x       ,          x           ,          x           ,       x        ],
            ['docId'           ,       x        ,      x       ,       x       ,                      ,                      ,       x        ],
        ]);
    }
});