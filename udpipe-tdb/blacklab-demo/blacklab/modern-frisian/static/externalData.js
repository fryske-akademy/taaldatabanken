if (window.vuexModules.corpus) {
    vuexModules.ui.getState().search.shared.customAnnotations.lemma = {
        render(config, state, Vue) {
            return new Vue({
                ...Vue.compile(`
                <div class="form-group propertyfield">
                    <label for="Generic_aspects_annotations/contents/lemma_value" title="lemmas" class="col-xs-12 col-md-3">Lemma <!----></label>
                    <div class="col-xs-12 col-md-9">
                        <div class="input-group">
                            <input id="Generic_aspects_annotations/contents/lemma_value" autocomplete="true" type="text" placeholder="Lemma" v-model="state.value" class="form-control">
                            <div class="input-group-btn">
                                <label for="Generic_aspects_annotations/contents/lemma_file" class="btn btn-default file-input-button">
                                    <span class="fa fa-upload fa-fw"></span>
                                    <input type="file" title="Upload a list of values" id="Generic_aspects_annotations/contents/lemma_file">
                                </label>
                            </div>
                        </div>
                        <div class="checkbox">
                            <label for="Generic_aspects_annotations/contents/lemma_case">
                                <input id="Generic_aspects_annotations/contents/lemma_case" type="checkbox" v-model="state.case">Case- and diacritics-sensitive
                            </label>
                            <button type="button" class="btn btn-sm btn-primary" v-on:click="dutchToFry()"
                             title="translate Dutch to Frisian, then click search">Translate dutch</button>
                        </div>
                    </div>
                </div>
            `),
                data: {
                    state,
                    config
                },
                methods: {
                    dutchToFry: function() {
                        if (this.state.value.includes('|')) return;
                        langapi.nlToFry(this.state);
                    }
                }
            })
        },
    }
}