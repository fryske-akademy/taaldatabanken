<xsl:transform xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="3.0">

  <xsl:include href="sentence-to-tei.xsl"/>

  <xsl:template match="@when">
    <xsl:attribute name="when">
      <xsl:value-of select="current-dateTime()"/>
    </xsl:attribute>
  </xsl:template>

  <xsl:template match="tei:ab">
    <xsl:param name="lines"/>
    <xsl:copy>
      <xsl:if test="empty($lines)"><noConlluToParse/></xsl:if>
      <xsl:for-each-group select="$lines" group-ending-with=".[.='']">
        <xsl:variable name="v">
          <_ sent_id="{substring(current-group()[starts-with(.,'# sent_id = ')][1],13)}">
            <xsl:for-each select="current-group()[contains(.,'&#9;')]">
              <xsl:variable name="t" select="tokenize(.,'&#9;_?')"/>
              <_ id="{$t[1]}" form="{$t[2]}" lemma="{$t[3]}" upostag="{$t[4]}" feats="{$t[6]}" head="{$t[7]}" deprel="{$t[8]}" misc="{$t[10]}"/>
            </xsl:for-each>
          </_>
        </xsl:variable>
        <xsl:apply-templates select="$v/*"/>
      </xsl:for-each-group>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="@*|node()" priority="-1">
    <xsl:param name="lines"/>
    <xsl:copy>
      <xsl:apply-templates select="@*|node()">
        <xsl:with-param name="lines" select="$lines"/>
      </xsl:apply-templates>
    </xsl:copy>
  </xsl:template>

</xsl:transform>
