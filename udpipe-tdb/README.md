# XSLT to integrate UDPipe and TEI

convert UDPipe output (CoNLL-U) into TEI XML

The TEI output is a [customized variant of TEI](https://bitbucket.org/fryske-akademy/tei-encoding) that can be validated.

## NOTE xslt maintained and tested here: https://bitbucket.org/fryske-akademy/blacklab/src/master/udpipe-plugin/src/main/resources/conlluXslt/

## Usage
### NOTE the stylesheet itself is input that is not processed by the stylesheet and template= parameter is optional

```
java net.sf.saxon.Transform "collection-uri=.?select=*.conllu;recurse=yes" conllu-to-tei.xsl conllu-to-tei.xsl template=tei-template.xml
```
or
```
java net.sf.saxon.Transform conllu-to-tei-streaming.xsl conllu-to-tei-streaming.xsl conllu=test.conllu
```
